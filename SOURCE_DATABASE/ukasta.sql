/*
 Navicat Premium Data Transfer

 Source Server         : Kearifan Local
 Source Server Type    : MySQL
 Source Server Version : 100432 (10.4.32-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : ukasta

 Target Server Type    : MySQL
 Target Server Version : 100432 (10.4.32-MariaDB)
 File Encoding         : 65001

 Date: 24/02/2024 18:00:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- ----------------------------
-- Table structure for mobil
-- ----------------------------
DROP TABLE IF EXISTS `mobil`;
CREATE TABLE `mobil`  (
  `iduser` int NULL DEFAULT NULL,
  `merek` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `plat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `sewa` int NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`plat`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mobil
-- ----------------------------
INSERT INTO `mobil` VALUES (170, 'Avanza', 'fufu', 'b1', 25000, '2024-02-24 11:16:23', '2024-02-24 11:16:23');
INSERT INTO `mobil` VALUES (1, 'Sigra', 'sese', 'b2', 15000, '2024-02-24 11:16:23', '2024-02-24 11:16:23');
INSERT INTO `mobil` VALUES (1, 'daihatsu', 'xenia', 'bx09099', 25000, '2024-02-24 09:57:32', '2024-02-24 09:57:32');
INSERT INTO `mobil` VALUES (171, 'punyoden', 'bibi', 'by23423432', 25000, '2024-02-24 10:49:42', '2024-02-24 10:49:42');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token` ASC) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type` ASC, `tokenable_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for trx_pengembalian
-- ----------------------------
DROP TABLE IF EXISTS `trx_pengembalian`;
CREATE TABLE `trx_pengembalian`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `idtrx` int NULL DEFAULT NULL,
  `totalbiaya` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `bukti` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_pengembalian
-- ----------------------------
INSERT INTO `trx_pengembalian` VALUES (5, 3, '50000', 'menunggu', '1708766826.png', '2024-02-24 09:27:06', '2024-02-24 09:27:06');

-- ----------------------------
-- Table structure for trx_sewa
-- ----------------------------
DROP TABLE IF EXISTS `trx_sewa`;
CREATE TABLE `trx_sewa`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `plat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `iduser` int NULL DEFAULT NULL,
  `mulai` datetime NULL DEFAULT NULL,
  `akhir` datetime NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_sewa
-- ----------------------------
INSERT INTO `trx_sewa` VALUES (3, 'b1', 1, '2024-02-22 00:00:00', '2024-02-23 00:00:00', '2024-02-24 04:55:42', '2024-02-24 05:05:34');
INSERT INTO `trx_sewa` VALUES (4, 'b1', 1, '2024-02-24 00:00:00', '2024-02-26 00:00:00', '2024-02-24 05:07:22', '2024-02-24 05:07:22');
INSERT INTO `trx_sewa` VALUES (7, 'by23423432', 1, '2024-02-24 00:00:00', '2024-02-25 00:00:00', '2024-02-24 10:54:34', '2024-02-24 10:54:34');
INSERT INTO `trx_sewa` VALUES (8, 'by23423432', 1, '2024-02-11 00:00:00', '2024-02-17 00:00:00', '2024-02-24 10:55:11', '2024-02-24 10:55:11');
INSERT INTO `trx_sewa` VALUES (9, 'by23423432', 171, '2024-02-04 00:00:00', '2024-02-10 00:00:00', '2024-02-24 10:55:56', '2024-02-24 10:55:56');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('user','admin') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'user',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nomorhp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `sim` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 172 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'basa bisi', 'admin', 'super@nova.com', '2023-05-04 07:04:47', '$2y$10$h5iu6RJVAW/hCnCOMEPq4u9PjZjAOX2mgy6PhyQxc3g9TdvG409fO', 'tyAjSHYtxY0IzSfggrdgGaR2LGxThWPVWohCwPLtvkCpOn366etJNiW10cl2', '2023-05-04 07:04:47', '2024-02-24 10:20:58', 'fafafa', '08234234', '6766666666666666');
INSERT INTO `users` VALUES (168, 'kaka', 'user', 'kaka@gmail.com', NULL, '$2y$10$h5iu6RJVAW/hCnCOMEPq4u9PjZjAOX2mgy6PhyQxc3g9TdvG409fO', NULL, '2023-10-13 21:16:48', '2023-10-13 21:16:48', NULL, '080808', '678678');
INSERT INTO `users` VALUES (169, 'mukelu bocet', 'user', 'joko@ono.com', NULL, '$2y$10$h5iu6RJVAW/hCnCOMEPq4u9PjZjAOX2mgy6PhyQxc3g9TdvG409fO', NULL, '2023-10-17 02:37:14', '2023-10-17 02:37:14', NULL, '081313131', '44444');
INSERT INTO `users` VALUES (170, 'coba', 'user', 'coba@gmail.com', NULL, '$2y$10$h5iu6RJVAW/hCnCOMEPq4u9PjZjAOX2mgy6PhyQxc3g9TdvG409fO', NULL, '2023-10-20 22:32:35', '2023-10-20 22:32:35', NULL, '08878787878', '345345345');
INSERT INTO `users` VALUES (171, 'baruu', 'user', 'baru@gmail.com', NULL, '$2y$10$h5iu6RJVAW/hCnCOMEPq4u9PjZjAOX2mgy6PhyQxc3g9TdvG409fO', NULL, '2023-10-21 09:16:57', '2024-02-24 10:49:24', 'fafafafafa', '12312312', '352423423');

SET FOREIGN_KEY_CHECKS = 1;
