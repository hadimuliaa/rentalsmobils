<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

if (!function_exists('hadi')) {


    function token()
    {
        $curl = curl_init();
        $das = "grant_type=client_credentials&client_id=wozpGdJ1hyf5WW7w5LhxWtMhEi0a&client_secret=2uxkqwiNx_e9Wt1gWmn5ryXWa0Qa";
        $data2 = array(
            'Content-Type : application/x-www-form-urlencoded',
        );

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://gateway.egw.xl.co.id/token",
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $data2,
            CURLOPT_POSTFIELDS => $das,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
        ));

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $err = curl_error($curl);
        $res = json_decode($response);

        curl_close($curl);
        if ($err) {
            $das = array(
                'p' => $httpCode,
                'n' => "cURL Error #:" . $err,
            );
        } else if ($httpCode != 200) {
            $das = array(
                'p' => $httpCode,
                'n' => 'Terjadi Kesalahan dengan Token : ' . $res->error_description,
            );
        } else {
            $das = array(
                'p' => $httpCode,
                'n' => $res->access_token,
            );
        }
        return $das;
    }
}
