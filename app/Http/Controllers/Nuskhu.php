<?php

namespace App\Http\Controllers;

use App\Models\Uka_product;
use App\Models\Uka_trxMobil;
use App\Models\Uka_trxkembalian;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Rap2hpoutre\FastExcel\Facades\FastExcel;
use Illuminate\Support\Facades\Validator;

class Nuskhu extends Controller
{
    // -------------------
    public function utamin()
    {
        $cre = '';
        return view('damin')->with('cred', $cre);
    }
    public function index()
    {
        $cre = '';
        return view('dashboard')->with('cred', $cre);
    }

    public function pelanggan(Request $request)
    {
        $up = new Uka_product();
        $h = $up->getUserPribadi(Auth::id());
        return view('pelanggan')->with([
            'urang' => $h,
        ]);
    }
    public function kembalian(Request $request)
    {
        $plat = $request->input('plat') ?? 0;
        $up = new Uka_product();
        $h = $up->getSewa(Auth::id(), $plat);
        return view('pengembalian')->with([
            'product' => $h,
            'userid' => Auth::id(),
        ]);
    }

    public function add_prodsewa_act(Request $request)
    {
        // Validasi data yang di-request oleh pengguna
        $validator = Validator::make($request->all(), [
            'idmobil' => 'required',
            'mulai' => 'required|date',
            'akhir' => 'required|date|after:mulai',
            'iduser' => 'required',
        ]);

        // Jika validasi gagal, kirim kembali ke halaman sebelumnya dengan pesan error
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        // Pengecekan apakah rentang waktu benturan dengan data yang sudah ada di database
        $existingBooking = Uka_trxMobil::where('plat', $request->input('idmobil'))
            ->where(function ($query) use ($request) {
                $query->whereBetween('mulai', [$request->input('mulai'), $request->input('akhir')])
                    ->orWhereBetween('akhir', [$request->input('mulai'), $request->input('akhir')]);
            })
            ->first();

        if ($existingBooking) {
            // Jika rentang waktu berbenturan, kirim pesan error dan kembali ke halaman sebelumnya
            return redirect()->back()->with('error', 'Maaf, mobil tidak dapat disewa pada rentang waktu tersebut. Silakan pilih tanggal lain.')->withInput();
        }

        // Jika tidak ada benturan, simpan data
        Uka_trxMobil::create([
            'plat' => $request->input('idmobil'),
            'mulai' => $request->input('mulai'),
            'akhir' => $request->input('akhir'),
            'iduser' => $request->input('iduser'),
        ]);

        return redirect()->back()->with('sukses', 'Data berhasil disimpan!');
    }
    public function add_mobil_act(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'model' => 'required',
            'plat' => 'required',
            'sewa' => 'required|numeric', // Sesuaikan dengan kebutuhan validasi
            'iduser' => 'required',
        ]);

        // Simpan data ke dalam model Uka_product
        $product = new Uka_product;
        $product->iduser = $request->iduser;
        $product->merek = $request->title;
        $product->model = $request->model;
        $product->plat = $request->plat;
        $product->sewa = $request->sewa;
        $product->save();


        return redirect()->back()->with('sukses', 'Data berhasil disimpan!');
    }

    public function kembalian_act(Request $request)
    {
        // Validasi form jika diperlukan
        $request->validate([
            'bukti' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', // Sesuaikan dengan kebutuhan validasi
            'idtrx' => 'required',
        ]);

        // Simpan bukti pembayaran ke dalam database
        $buktiPembayaran = new Uka_trxkembalian;
        $buktiPembayaran->idtrx = $request->idtrx;
        $buktiPembayaran->totalbiaya = $request->totalbiaya;
        $buktiPembayaran->status = 0;

        // Upload gambar bukti pembayaran
        if ($request->hasFile('bukti')) {
            $image = $request->file('bukti');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('berkas'), $imageName);
            $buktiPembayaran->bukti = $imageName;
        }

        $buktiPembayaran->save();

        // Tambahkan logika atau redirect sesuai kebutuhan
        return redirect()->route('kembalian')->with('sukses', 'Pengembalian telah dilakukan, mohon menunggu, hingga transaksi mu di setujui admin');
    }

    public function product(Request $request)
    {
        $daterange = $request->input('daterange');
        $merek = $request->input('merek');
        $model = $request->input('model');
        $avail = $request->input('avail');
        $up = new Uka_product();
        $product = $up->getDataM($merek, $model, $daterange, $avail);
        $merk = $up->getMerek();
        $modl = $up->getModel();
        $getMobils = $up->getMobils();
        return view('product')->with([
            'product' => $product,
            'merk' => $merk,
            'modl' => $modl,
            'mobls' => $getMobils,
            'userid' =>  Auth::id(),

        ]);
    }

    public function user_act(Request $request)
    {
        $id = $request->input('idseo');

        // Update data in User model
        $userData = [
            'name' => $request->filled('namatoko') ? $request->input('namatoko') : null,
            'alamat' => $request->filled('alamat') ? $request->input('alamat') : null,
            'nomorhp' => $request->filled('nomorhp') ? $request->input('nomorhp') : null,
            'sim' => $request->filled('sim') ? $request->input('sim') : null,
        ];

        if ($request->filled('pwd')) {
            $userData['password'] = Hash::make($request->input('pwd'));
        }

        User::where('id', $id)->update(array_filter($userData, 'strlen'));

        return redirect()->back()->with('sukses', 'Data updated successfully');
    }
}
