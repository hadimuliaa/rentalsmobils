<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Otent extends Controller
{
    public function login()
    {
        return view('login');
    }
    public function cek(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            if (Auth::user()->isAdmin()) {
                return redirect('/utamin')->with('sukses', 'Alhamdulillah, Anda Berhasil Login.');
            } else {
                return redirect('/utamin')->with('sukses', 'Alhamdulillah, Anda Berhasil Login');
            }
        }
        return redirect()->intended('/');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/')->with('sukses', 'Alhamdulillah, Anda Berhasil Logout.');
    }
}
