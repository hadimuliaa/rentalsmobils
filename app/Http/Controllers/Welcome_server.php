<?php

namespace App\Http\Controllers;

use App\Models\Uka_product;
use App\Models\Uka_billingAddres;
use App\Models\Uka_chekcout;
use App\Models\Uka_pemilik;
use App\Models\Uka_range;
use App\Models\User;
use App\Models\Uka_cart;
use App\Models\Uka_kurir;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Rap2hpoutre\FastExcel\Facades\FastExcel;

class Welcome extends Controller
{
    private $berapi = '1ef4d7e603cae3f964205c5e1a3f4da0'; //akunpro bayan
    // private $berapi = '365649754ebdf2c8002aa4a9ee0d25e0';
    public function processOrder(Request $request)
    {
        try {
            // Validasi request
            $request->validate([
                'grandTotal' => 'required|numeric',
                'shippingFee' => 'required|numeric',
                'kurir' => 'required|string',
                'ongs' => 'required|string',
                'subTotal' => 'required|numeric',
                'isi_keranjang' => 'required|string',
            ], [
                'grandTotal.required' => 'Kolom Grand Total harus diisi.',
                'grandTotal.numeric' => 'Kolom Grand Total harus berupa angka.',

                'shippingFee.required' => 'Kolom Shipping Fee harus diisi.',
                'shippingFee.numeric' => 'Kolom Shipping Fee harus berupa angka.',

                'kurir.required' => 'Kolom Kurir harus diisi.',
                'kurir.string' => 'Kolom Kurir harus berupa teks.',

                'ongs.required' => 'Kolom Ongkos Kirim harus diisi.',
                'ongs.string' => 'Kolom Ongkos Kirim harus berupa teks.',

                'subTotal.required' => 'Kolom Sub Total harus diisi.',
                'subTotal.numeric' => 'Kolom Sub Total harus berupa angka.',

                'isi_keranjang.required' => 'Isi Keranjang harus diisi.',
                'isi_keranjang.string' => 'Isi Keranjang harus berupa teks.',
            ]);


            // Ambil data dari request
            $grandTotal = $request->input('grandTotal');
            $shippingFee = $request->input('shippingFee');
            $kurir = $request->input('kurir');
            $ongs = $request->input('ongs');
            $subTotal = $request->input('subTotal');
            $isi_keranjang = $request->input('isi_keranjang');
            $userId = Auth::id(); // Anda dapat mengganti ini sesuai kebutuhan
            $inv = 'INV' . time();

            // Simpan data ke dalam model
            $checkout = new Uka_chekcout();
            $checkout->invoice = $inv; // Sesuaikan dengan kebutuhan
            $checkout->iduser = $userId;
            $checkout->kurir = $kurir;
            $checkout->kurir_paket = $ongs;
            $checkout->jumlah = $subTotal;
            $checkout->ongkir = $shippingFee;
            $checkout->grandtotal = $grandTotal;
            $checkout->save();

            // Decode JSON dari isi_keranjang
            $cartData = json_decode($isi_keranjang, true);
            foreach ($cartData as $c) {
                echo $c['idcart'];
                Uka_cart::where('id', $c['idcart'])->update(['invoice' => $inv]);
            }

            // Redirect ke route finish dengan membawa nilai invoice dan grandTotal
            return redirect()->route('finish', ['invoice' => $inv, 'grandTotal' => $grandTotal]);
        } catch (\Exception $e) {
            // Tangani kesalahan dan kembalikan respons yang sesuai
            return redirect()->back()->with('error', 'Mohon Lengkapi data yang ada, ' . $e->getMessage())->withInput();
        }
    }

    // $a1 = "Tindak%20Lanjuti%20Pesanan%20#" . $inv . "%0ABismillah,%20Assalamu'alaikum,,%0AHai%20Admin,%0AMohon%20perhatiannya%20terkait%20pesanan%20dengan%20nomor%20invoice%20" . $inv . "%20yang%20baru%20saya%20buat.%0ATolong%20segera%20diproses%20ya";
    public function checkOut(Request $request)
    {
        $inv = $request->invoice ?? '';
        $uc = new Uka_cart();
        $ubil = new Uka_billingAddres();
        $us = new User();
        $p = new Uka_product();
        $auth = Auth::user();
        $ba = $ubil->BilingAdres();
        // $cost = $this->calculateShippingCost($ubil->BilingAdres());
        // dd($cost);

        if (!$ba) {
            $l = "nuskhuDigital";
            return redirect()->route('dashboa.fe', ['idbil' => $l])->with('info', 'Mohon lengkapi terlebih dahulu alamatnya.');
        }
        return view('\fe\checkout')->with([
            'daus' => $auth,
            'inv' => $inv,
            'prod' => $p->getData(3, ''),
            'profil' => $us->getData($auth->id),
            'total_keranjang' => $uc->getData(),
            'ba' => $ubil->BilingAdres(),
            'kurir' => Uka_kurir::all(),
            'isi_keranjang' => $uc->getIsi($inv),
            'd_keranjang' => $uc->getDataLahLapeh($inv),
        ]);
    }
    public function finish(Request $request)
    {
        $uc = new Uka_cart();
        $auth = Auth::user();
        $p = new Uka_product();
        return view('\fe\finish')->with([
            // 'whatsappLink' => $whatsappLink,
            'daus' => $auth,
            'prod' => $p->getData(3, ''),
            'inv' => $request->invoice,
            'total' => $request->grandTotal,
            'total_keranjang' => $uc->getData(),
        ]);
    }
    public function calculateShippingCost(Request $request)
    {
        $pemilik = Uka_pemilik::first();
        $berapi = $pemilik->berapi;
        // $asal = 501;
        // $idkota_terpilih = 574;
        // $barek = 1700;
        $asal = $pemilik->kodeSubsdistrict; // Ganti dengan asal yang sesuai
        $idkota_terpilih = $request->idkota;
        $barek = $request->barek;
        $kurir = $request->kurir;

        $response = Http::withHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded',
            'key' => $berapi,
        ])
            ->asForm()
            ->post('https://pro.rajaongkir.com/api/cost', [
                'origin' => $asal,
                'originType' => 'subdistrict',
                'destination' => $idkota_terpilih,
                'destinationType' => 'city',
                'weight' => $barek,
                'courier' => $kurir, // Ganti dengan ekspedisi_terpilih
            ]);
        // $responseData = $response->json();
        // dd($responseData);

        $datajasa = $response->json()['rajaongkir']['results'][0]['costs'];

        $options = "<option value=''>--Pilih Jasa--</option> ";

        foreach ($datajasa as $key => $v) {
            $options .= "<option value='" . $v['service'] . "' ongkir='" . $v['cost'][0]['value'] . "' serpis='" . $v['service'] . "'>" . $v['service'] . " | Rp." .  number_format($v['cost'][0]['value']) . ",- | " . $v['cost'][0]['etd'] . " Hari </option>";
        }

        return response()->json(['options' => $options]);
    }
    public function simpanAlamat(Request $request)
    {
        $ubil = new Uka_billingAddres();
        if ($ubil->BilingAdres()) {
            $u = Auth::user();
            $ubil = Uka_billingAddres::where('iduser', $u->id)->first();
            $ubil->delete();
        }
        try {
            // Validasi data jika diperlukan
            $request->validate([
                'provinsi' => 'required',
                'kota' => 'required',
                'kecamatan' => 'required',
                'alamat' => 'required',
            ]);

            // dd($request);
            $u = Auth::user();
            // Simpan data ke database
            // Ambil nama provinsi, kota, dan kecamatan dari request
            $namaProvinsi = $request->input('n_prov');
            $namaKota = $request->input('n_kota');
            $namaKecamatan = $request->input('n_kec');

            // Simpan data ke database
            $alamat = new Uka_billingAddres;
            $alamat->iduser = $u->id; // Sesuaikan dengan cara Anda mendapatkan ID user
            $alamat->provinsi = $request->input('provinsi');
            $alamat->kota = $request->input('kota');
            $alamat->kecamatan = $request->input('kecamatan');
            $alamat->alamat = $request->input('alamat');

            // Simpan juga nama provinsi, kota, dan kecamatan
            $alamat->n_prov = $namaProvinsi;
            $alamat->n_kota = $namaKota;
            $alamat->n_kec = $namaKecamatan;
            $alamat->save();

            return redirect()->back()->with('sukses', 'Alamat berhasil disimpan.', 'Alhamdulillah..');
        } catch (\Illuminate\Validation\ValidationException $e) {
            return redirect()->back()->withErrors($e->errors())->withInput();
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Terjadi kesalahan: ' . $e->getMessage())->withInput();
        }
    }


    public function getSubdistrict(Request $request)
    {
        $idKota = $request->input('idkota');
        $response = Http::withHeaders(['key' => $this->berapi])
            ->get("https://pro.rajaongkir.com/api/subdistrict?city=" . $idKota);
        $data = $response->json()['rajaongkir']['results'];

        $options = "<option value=''>--Pilih Subdistrict--</option>";

        foreach ($data as $item) {
            $options .= "<option value='" . $item['subdistrict_id'] . "' idsubdistrict='" . $item['subdistrict_id'] . "'>" . $item['subdistrict_name'] . "</option>";
        }

        return response()->json(['options' => $options]);
    }
    public function provinsi()
    {
        $response = Http::withHeaders(['key' => $this->berapi])
            ->get("https://pro.rajaongkir.com/api/province");

        $data = $response->json()['rajaongkir']['results'];

        $options = "<option value=''>--Pilih Provinsi--</option>";

        foreach ($data as $item) {
            $options .= "<option value='" . $item['province_id'] . "' idprovinsi='" . $item['province_id'] . "'>" . $item['province'] . "</option>";
        }

        return $options;
    }

    public function getKota(Request $request)
    {
        $idProvinsi = $request->input('idprovinsi');
        $response = Http::withHeaders(['key' => $this->berapi])
            ->get("https://pro.rajaongkir.com/api/city?province=" . $idProvinsi);
        $data = $response->json()['rajaongkir']['results'];

        $options = "<option value=''>--Pilih Kota--</option>";

        foreach ($data as $item) {
            $options .= "<option value='" . $item['city_id'] . "' idkota='" . $item['city_id'] . "'>" . $item['city_name'] . "</option>";
        }

        return response()->json(['options' => $options]);
    }

    public function index()
    {
        $up = new Uka_product();
        $uc = new Uka_cart();
        $product = $up->getData(1, '');
        $section = $up->getData(2, '');
        return view('fe.mainFe')->with([
            'section' => $section,
            'prod' => $up->getData(3, ''),
            'product' => $product,
            'daus' => Auth::user(),
            'total_keranjang' => $uc->getData(),
        ]);
    }
    public function removeCartItem($idcart)
    {
        // Ambil id pengguna yang sedang masuk
        $userId = Auth::id();

        // Temukan item keranjang berdasarkan idcart, iduser, dan hapus
        Uka_cart::where('id', $idcart)
            ->where('iduser', $userId)
            ->delete();

        // Redirect kembali dengan pesan sukses
        return redirect()->back()->with('sukses', 'Alhamdulillah,Produk berhasil dihapus dari keranjang.');
    }
    public function updateCartQuantity_(Request $request)
    {
        $up = new Uka_range();
        $ure = Uka_range::all();
        // Validasi request
        $request->validate([
            'product_id' => 'required|array',
            'new_quantity' => 'required|array',
            'idcat' => 'required|array',
        ]);

        // Ambil id pengguna yang sedang masuk
        $userId = Auth::id();
        // Loop melalui produk yang akan diperbarui
        foreach ($request->input('product_id') as $index => $productId) {
            // Ambil item keranjang berdasarkan produk dan pengguna
            $cartItem = Uka_cart::where('idprod', $productId)
                ->where('iduser', $userId)
                ->first();

            // Jika item keranjang tidak ditemukan, lanjutkan ke produk berikutnya
            if (!$cartItem) {
                continue;
            }

            // Perbarui jumlah item keranjang sesuai request
            $cartItem->jumlah = $request->input('new_quantity')[$index];
            // Hitung total harga baru
            $cartItem->range = 0;
            if ($request->input('idcat')[$index] == 1) {
                if ($cartItem->jumlah >= 12 && $cartItem->jumlah <= 50) {
                    $cartItem->hargaSatuan = 27000;
                    $cartItem->range = 1;
                } elseif ($cartItem->jumlah >= 51 && $cartItem->jumlah <= 99) {
                    $cartItem->range = 1;
                    $cartItem->hargaSatuan = 26000;
                } elseif ($cartItem->jumlah >= 100 && $cartItem->jumlah <= 150) {
                    $cartItem->hargaSatuan = 25000;
                    $cartItem->range = 1;
                } elseif ($cartItem->jumlah >= 151) {
                    $cartItem->hargaSatuan = 24000;
                    $cartItem->range = 1;
                } else {
                    // Jika tidak ada range yang cocok, gunakan harga biasa
                    $cartItem->hargaSatuan = $request->input('hargaprod')[$index];
                }
            } elseif ($request->input('idcat')[$index] == 2) {
                if ($cartItem->jumlah >= 12 && $cartItem->jumlah <= 60) {
                    $cartItem->hargaSatuan = 29000;
                    $cartItem->range = 1;
                } elseif ($cartItem->jumlah >= 61 && $cartItem->jumlah <= 120) {
                    $cartItem->hargaSatuan = 28500;
                    $cartItem->range = 1;
                } elseif ($cartItem->jumlah >= 121 && $cartItem->jumlah <= 420) {
                    $cartItem->hargaSatuan = 27500;
                    $cartItem->range = 1;
                } elseif ($cartItem->jumlah >= 421 && $cartItem->jumlah <= 600) {
                    $cartItem->hargaSatuan = 26500;
                    $cartItem->range = 1;
                } elseif ($cartItem->jumlah >= 601) {
                    $cartItem->hargaSatuan = 26000;
                    $cartItem->range = 1;
                } else {
                    // Jika tidak ada range yang cocok, gunakan harga biasa
                    $cartItem->hargaSatuan = $request->input('hargaprod')[$index];
                }
            }

            // Update total harga
            $cartItem->harga = $cartItem->jumlah * $cartItem->hargaSatuan;

            $cartItem->save();
        }

        // Redirect kembali dengan pesan sukses
        return redirect()->back()->with('sukses', 'Jumlah produk berhasil diperbarui.', 'Alhamdulillah..');
    }
    public function updateCartQuantity_oldpagi(Request $request)
    {
        // Validasi request
        $request->validate([
            'product_id' => 'required|array',
            'new_quantity' => 'required|array',
            'idcat' => 'required|array',
        ]);

        // Ambil id pengguna yang sedang masuk
        $userId = Auth::id();

        // Loop melalui produk yang akan diperbarui
        foreach ($request->input('product_id') as $index => $productId) {
            // Ambil item keranjang berdasarkan produk dan pengguna
            $cartItem = Uka_cart::where('idprod', $productId)
                ->where('iduser', $userId)
                ->where('invoice', null)
                ->first();
            // Jika item keranjang tidak ditemukan, lanjutkan ke produk berikutnya
            if (!$cartItem) {
                continue;
            }

            // Perbarui jumlah item keranjang sesuai request
            $cartItem->jumlah = $request->input('new_quantity')[$index];

            // Hitung total harga baru
            $cartItem->range = 0;

            // Ambil data dari model Uka_range untuk kategori yang sesuai
            $rangeData = Uka_range::where('categori', $request->input('idcat')[$index])->get();

            foreach ($rangeData as $range) {
                if ($cartItem->jumlah >= $range->range_start && $cartItem->jumlah <= $range->range_end) {
                    $cartItem->hargaSatuan = $range->harganew;
                    $cartItem->range = 1;
                    break; // Keluar dari loop setelah menemukan range yang cocok
                }
            }

            // Jika tidak ada range yang cocok, gunakan harga biasa
            if (!isset($cartItem->range)) {
                $cartItem->hargaSatuan = $request->input('hargaprod')[$index];
            }

            // Update total harga
            $cartItem->harga = $cartItem->jumlah * $cartItem->hargaSatuan;

            $cartItem->save();
        }


        // Redirect kembali dengan pesan sukses
        return redirect()->back()->with('sukses', 'Jumlah produk berhasil diperbarui.', 'Alhamdulillah..');
    }
    public function updateCartQuantity(Request $request)
    {
        // Validasi request
        $request->validate([
            'product_id' => 'required|array',
            'new_quantity' => 'required|array',
            'idcat' => 'required|array',
        ]);

        // Ambil id pengguna yang sedang masuk
        $userId = Auth::id();

        // Loop melalui produk yang akan diperbarui
        foreach ($request->input('product_id') as $index => $productId) {
            // Ambil item keranjang berdasarkan produk dan pengguna
            $cartItem = Uka_cart::where('idprod', $productId)
                ->where('iduser', $userId)
                ->where('invoice', null)
                ->first();
            // Jika item keranjang tidak ditemukan, lanjutkan ke produk berikutnya
            if (!$cartItem) {
                continue;
            }

            // Perbarui jumlah item keranjang sesuai request
            $cartItem->jumlah = $request->input('new_quantity')[$index];

            // Hitung total harga baru
            $cartItem->range = 0;

            // Ambil data dari model Uka_range untuk kategori yang sesuai
            $rangeData = Uka_range::where('categori', $request->input('idcat')[$index])->get();

            // Kondisi khusus untuk "Diatas 150"
            if ($cartItem->jumlah > 150 && $request->input('idcat')[$index] == 1) {
                $diatas150 = $rangeData->where('range', 'Diatas 150')->first();
                if ($diatas150) {
                    $cartItem->hargaSatuan = $diatas150->harganew;
                    $cartItem->range = 1;
                }
            } else if ($cartItem->jumlah > 601 && $request->input('idcat')[$index] == 2) {
                $diatas601 = $rangeData->where('range', 'Diatas 601')->first();
                if ($diatas601) {
                    $cartItem->hargaSatuan = $diatas601->harganew;
                    $cartItem->range = 1;
                }
            } else {
                // Kondisi untuk rentang harga lainnya
                foreach ($rangeData as $range) {
                    if ($cartItem->jumlah >= $range->range_start && $cartItem->jumlah <= $range->range_end) {
                        $cartItem->hargaSatuan = $range->harganew;
                        $cartItem->range = 1;
                        break; // Keluar dari loop setelah menemukan range yang cocok
                    }
                }
            }

            // Jika tidak ada range yang cocok, gunakan harga biasa
            if (!isset($cartItem->range)) {
                $cartItem->hargaSatuan = $request->input('hargaprod')[$index];
            }

            // Update total harga
            $cartItem->harga = $cartItem->jumlah * $cartItem->hargaSatuan;

            $cartItem->save();
        }


        // Redirect kembali dengan pesan sukses
        return redirect()->back()->with('sukses', 'Jumlah produk berhasil diperbarui.', 'Alhamdulillah..');
    }

    public function addToCart(Request $request)
    {
        $productId = $request->productId;
        // Ambil produk berdasarkan $productId
        $product = Uka_product::find($productId);

        // Pastikan produk ditemukan
        if (!$product) {
            return redirect()->back()->with('error', 'Maaf,Produk tidak ditemukan.');
        }

        // Ambil pengguna yang sedang masuk
        $user = Auth::user();

        // Buat atau temukan item keranjang untuk produk ini dan pengguna
        $cartItem = Uka_cart::where('idprod', $product->id)
            ->where('iduser', $user->id)
            ->where('invoice', null)
            ->first();


        // Jika item keranjang tidak ditemukan, buat yang baru
        if (!$cartItem) {
            $cartItem = new Uka_cart();
            $cartItem->idprod = $product->id;
            $cartItem->iduser = $user->id;
            $cartItem->jumlah = 1;
            $cartItem->harga = $product->harga;
            $cartItem->hargaSatuan = $product->harga;
        } else {
            // Jika item keranjang sudah ada, tambahkan jumlah atau lakukan operasi lain sesuai kebutuhan
            $cartItem->jumlah += 1;
            $cartItem->harga += $product->harga;
        }

        // Simpan perubahan atau item baru
        $cartItem->save();

        return redirect()->back()->with('sukses', 'Alhamdulillah,,,Produk berhasil ditambahkan ke keranjang.');
    }

    public function showRegistrationForm()
    {
        $up = new Uka_product();
        $uc = new Uka_cart();
        $product = $up->getData(1, '');
        $section = $up->getData(2, '');
        return view('fe.reg')->with([
            'section' => $section,
            'product' => $product,
            'prod' => $up->getData(3, ''),
            'daus' => Auth::user(),
            'total_keranjang' => $uc->getData(),
        ]);
    }
    public function updateProfile(Request $request)
    {
        // Validasi data update profil
        $request->validate([
            'display_name' => 'required',
            'email' => 'required|email|unique:users,email,' . Auth::user()->id,
            'new_password' => 'nullable|min:6',
        ]);

        // Ambil pengguna yang sedang masuk
        $user = Auth::user();

        // Perbarui nama dan email
        $user->name = $request->input('display_name');
        $user->email = $request->input('email');

        // Perbarui kata sandi jika ada
        if ($request->filled('new_password')) {
            $user->password = Hash::make($request->input('new_password'));
        }

        // Simpan perubahan
        $user->save();

        // Redirect ke halaman profil dengan pesan sukses
        return redirect()->route('dashboa.fe')->with('sukses', 'Alhamdulillah,,,Profil berhasil diperbarui!');
    }
    public function dashboard_user(Request $request)
    {
        $diBil = $request->idbil;
        // dd($diBil);
        $uc = new Uka_cart();
        $p = new Uka_product();
        $us = new User();
        $ubil = new Uka_billingAddres();
        $auth = Auth::user();
        // dd($daus);
        return view('fe.dashboa')->with([
            'daus' => $auth,
            'profil' => $us->getData($auth->id),
            'total_keranjang' => $uc->getData(),
            'prod' => $p->getData(3, ''),
            'daftarTrx' => $uc->getDataLahLapeh(''),
            'provinsiOptions' => $this->provinsi(),
            'ba' => $ubil->BilingAdres(),
            'idBil' => $diBil,
        ]);
    }
    public function cartbuy()
    {
        $uc = new Uka_cart();
        $us = new User();
        $p = new Uka_product();
        $auth = Auth::user();
        // dd($daus);
        return view('fe.cartbuy')->with([
            'daus' => $auth,
            'profil' => $us->getData($auth->id),
            'total_keranjang' => $uc->getData(),
            'prod' => $p->getData(3, ''),
            'isi_keranjang' => $uc->getIsi(''),
        ]);
    }

    public function register(Request $request)
    {
        try {
            // Validasi data registrasi
            $request->validate([
                'name' => 'required',
                'nomorhp' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6',
            ]);
            // Simpan data registrasi ke database
            $user =  User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'nomorhp' => $request->input('nomorhp'),
                'password' => Hash::make($request->input('password')),
            ]);
            Auth::login($user);
            // Redirect ke halaman setelah registrasi berhasil
            return redirect()->route('dashboa.fe')->with('sukses', 'Alhamdulillah,,,Registrasi berhasil!');
        } catch (ValidationException $e) {
            // Tangkap kesalahan validasi
            return redirect()->back()->withErrors($e->errors())->withInput();
        } catch (\Exception $e) {
            // Tangkap kesalahan umum
            return redirect()->back()->with('error', 'Subhanallah,,,Terjadi kesalahan: ' . $e->getMessage())->withInput();
        }
    }
    public function mask(Request $request)
    {  // Validasi data login
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        // Coba melakukan login
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Jika login berhasil, redirect ke halaman yang diinginkan
            return redirect()->route('/')->with('sukses', 'Alhamdulillah,,,Login berhasil..!!');
        } else {
            // Jika login gagal, kembali ke halaman login dengan pesan error
            return redirect()->route('logins')->with('error', 'Subhanallah,,Login gagal. Periksa kembali username atau password.');
        }
    }
    public function logins()
    {
        $p = new Uka_product();
        $uc = new Uka_cart();
        return view('fe.login')->with([
            'daus' => Auth::user(),
            'prod' => $p->getData(3, ''),
            'total_keranjang' => $uc->getData(),
        ]);
    }

    public function blog(Request $request)
    {
        $paramValue = $request->has('param') ? $request->input('param') : '';
        $uc = new Uka_cart();
        $up = new Uka_product();
        $foto = $up->getFoto($paramValue);
        return view('fe.blog')->with([
            'foto' => $foto,
            'prod' => $up->getData(3, ''),
            'daus' => Auth::user(),
            'total_keranjang' => $uc->getData(),
        ]);
    }
    public function details($id)
    {
        $foto = '';
        $up = new Uka_product();
        $uc = new Uka_cart();
        $product = $up->getData(1, $id);
        return view('fe.det')->with([
            'product' => $product,
            'daus' => Auth::user(),
            'prod' => $up->getData(3, ''),
            'total_keranjang' => $uc->getData(),
        ]);
    }
}
