<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Uka_product extends Model
{
    protected $table = 'mobil';
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'iduser',
        'merek',
        'model',
        'plat',
        'sewa',
        'created_at',
        'updated_at',

    ];
    public function getDataM($idet, $model, $datee, $avail)
    {
        $key = '';
        $key2 = '';
        $key3 = '';
        $key4 = '';

        if ($idet) {
            $key = "and merek='$idet'";
        }

        if ($model) {
            $key2 = "and model='$model'";
        }

        if ($avail) {
            $key4 = "and stat='$avail'";
        }
        // Set nilai default untuk tanggal awal dan akhir
        $startDate = date('Y-m-01'); // Mulai dari awal bulan
        $endDate = date('Y-m-t');   // Sampai akhir bulan
        if ($datee) {
            // Memecah rentang tanggal menjadi tanggal awal dan akhir
            $dateRange = explode(' to ', $datee);

            $startDate = date('Y-m-d', strtotime($dateRange[0]));
            $endDate = date('Y-m-d', strtotime($dateRange[1]));
        }

        // Query untuk mendapatkan hasil yang tidak avail
        $queryNotAvail = DB::select("SELECT coalesce(idtrx,0) as idtrx ,plat,iduser, merek,model,sewa,stat,mulai, akhir
            from (SELECT ts.id as idts, m.plat,m.iduser, merek,model,sewa,1 as stat, ts.mulai, ts.akhir
            FROM mobil m
            LEFT JOIN trx_sewa ts ON m.plat = ts.plat
            AND (
                ('$endDate' >= ts.mulai AND '$startDate' <= ts.akhir)
            )
            WHERE ts.plat IS NOT NULL $key $key2)a left join trx_pengembalian b  on idtrx=idts where status=0 $key4
        ");

        // Query untuk mendapatkan hasil yang avail
        $queryAvail = DB::select("SELECT plat,iduser, merek,model,sewa,stat,mulai, akhir
            from (SELECT m.plat,m.iduser, merek,model,sewa,0 as stat,mulai, akhir
            FROM mobil m
            LEFT JOIN trx_sewa ts ON m.plat = ts.plat
            AND (
                ('$endDate' >= ts.mulai AND '$startDate' <= ts.akhir)
            )
            WHERE ts.plat IS NULL $key $key2)a where plat!='0' $key4
        ");

        // Menggabungkan hasil dari kedua query
        $result = array_merge($queryNotAvail, $queryAvail);

        return $result;
    }
    public function getMerek()
    {
        $result = DB::select("SELECT merek from mobil GROUP BY merek");
        return $result;
    }
    public function getModel()
    {
        $result = DB::select("SELECT model from mobil GROUP BY model");
        return $result;
    }
    public function getMobils()
    {
        $result = DB::select("SELECT plat,merek,model from mobil");
        return $result;
    }
    public function getSewa($id, $plat)
    {
        $key4 = '';

        if ($plat) {
            $key4 = "and ts.plat='$plat'";
        }
        $result = DB::select("SELECT idts,plat,status, iduser,mulai,akhir,merek,model, sewa,a.totalbiaya
        from (SELECT ts.id as idts, ts.plat, ts.iduser,mulai,akhir,merek,model, sewa,
             GREATEST(DATEDIFF(CURDATE(), mulai), 1) * sewa as totalbiaya
                 from trx_sewa ts, mobil m where ts.plat=m.plat and ts.iduser='$id' $key4)a LEFT JOIN trx_pengembalian pb on idts=idtrx ");
        return $result;
    }
    public function getUserPribadi($id)
    {
        $result = DB::select("SELECT id, name as nama,email,alamat,nomorhp,sim  from users where id='$id' ");
        return $result;
    }
}
