<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Uka_trxMobil extends Model
{
    protected $table = 'trx_sewa';
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'plat',
        'iduser',
        'mulai',
        'akhir',
        'created_at',
        'updated_at',

    ];
    public function getDataM($idet, $model)
    {
        $key = '';
        $key2 = '';
        if ($idet) {
            $key = "and merek='$idet'";
        }
        if ($model) {
            $key2 = "and model='$model'";
        }
        $query = DB::select("SELECT id,iduser, merek,model,plat,sewa from mobil where sewa!='0' $key $key2");

        return $query;
    }
    public function getData($tipe, $idet)
    {
        $key = '';
        if ($idet) {
            $key = "and a.id='$idet'";
        }
        $query = DB::select("SELECT idp,idf, idcat,b.name,namprod,harga,berat,a.desc,ukuran,file
        from (SELECT a.id as idp, b.id as idf, idcat,name as namprod,harga,berat,a.desc,ukuran,file from uka_product a, uka_foto b where a.id=idprod and tipe='$tipe' $key)a left join uka_categori b on id=idcat");

        return $query;
    }
    public function getFoto($id)
    {
        $key = '';
        if ($id) {
            $key = "and idcat='$id'";
        }
        $query = DB::select("select a.id,idcat,a.name,b.name as namkat from uka_knowledge a, uka_categori b where idcat=b.id $key");

        return $query;
    }
}
