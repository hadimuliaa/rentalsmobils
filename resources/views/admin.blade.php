@extends('template')
@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0">Admin</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Ecommerce</a></li>
                                    <li class="breadcrumb-item active">Admin</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="card">
                        <div class="card-body">
                            <div class="col-xxl-12">
                                @foreach ($urang as $p)
                                    <div class="card" id="contact-view-detail">
                                        <div class="card-body text-center">
                                            <div class="position-relative d-inline-block">
                                                <img src="{{ asset('assetsfe/images/' . $p->logo) }}" alt=""
                                                    class="avatar-lg rounded-circle img-thumbnail">
                                                <span
                                                    class="contact-active position-absolute rounded-circle bg-success"><span
                                                        class="visually-hidden"></span>
                                            </div>
                                            <h5 class="mt-4 mb-1">{{ $p->namaToko }}</h5>
                                            <p class="text-muted">Username : {{ $p->username }}</p>

                                            <ul class="list-inline mb-0">
                                                <li class="list-inline-item avatar-xs">
                                                    <a data-bs-toggle="modal"
                                                        data-bs-target="#exampleModalgrid_{{ $p->id }}"
                                                        class="btn btn-warning">Edit</a>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="card-body">
                                            <h6 class="text-muted text-uppercase fw-semibold mb-3">Alamat</h6>
                                            <p class="text-muted mb-4">{{ $p->Alamat }}.</p>
                                            <h6 class="text-muted text-uppercase fw-semibold mb-3">Nomor Rekening</h6>
                                            <p class="text-muted mb-4">{{ $p->norek }}.</p>
                                            <div class="table-responsive table-card">
                                                <table class="table table-borderless mb-0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="fw-medium" scope="row">Email Official</td>
                                                            <td>{{ $p->email }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="fw-medium" scope="row">Nomor Hp</td>
                                                            <td>{{ $p->hp }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="fw-medium" scope="row">Web Official</td>
                                                            <td>{{ $p->web }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="fw-medium" scope="row">Facebook Official</td>
                                                            <td>{{ $p->facebook }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="fw-medium" scope="row">Instagram Official</td>
                                                            <td>{{ $p->ig }}</td>
                                                        </tr>


                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end card-->
                            </div>
                            @endforeach
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end row -->

            </div>
            <!-- container-fluid -->
        </div>

        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
    </div>
    </div>
    @foreach ($urang as $is)
        <div class="modal fade" id="exampleModalgrid_{{ $is->id }}" tabindex="-1"
            aria-labelledby="exampleModalgridLabel" aria-modal="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ route('update_admin') }}" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">
                            @csrf
                            <div class="col-lg-12">
                                <div class="col-xxl-12">
                                    <div>
                                        <label for="firstName" class="form-label">Nama Toko</label>
                                        <input type="text" class="form-control" name="namatoko"
                                            value="{{ $is->namaToko }}">
                                        <input type="hidden" class="form-control" name="idseo"
                                            value="{{ $is->id }}">
                                    </div>
                                    <div>
                                        <label for="alamat" class="form-label">Alamat</label>
                                        <textarea class="form-control" name="alamat">{{ $is->Alamat }}</textarea>
                                    </div>
                                    <div>
                                        <label for="norek" class="form-label">Nomor Rekening</label>
                                        <textarea class="form-control" name="norek">{{ $is->norek }}</textarea>
                                    </div>

                                    <div>
                                        <label for="firstName" class="form-label">Instagram Official</label>
                                        <input type="text" class="form-control" name="ig"
                                            value="{{ $is->ig }}">
                                    </div>
                                    <div>
                                        <label for="firstName" class="form-label">Email Official</label>
                                        <input type="text" class="form-control" name="email"
                                            value="{{ $is->email }}">
                                    </div>
                                    <div>
                                        <label for="firstName" class="form-label">Website Official</label>
                                        <input type="text" class="form-control" name="web"
                                            value="{{ $is->ig }}">
                                    </div>
                                    <div>
                                        <label for="firstName" class="form-label">Facebook Official</label>
                                        <input type="text" class="form-control" name="facebook"
                                            value="{{ $is->facebook }}">
                                    </div>
                                    <div>
                                        <label for="firstName" class="form-label">Nomor HP *format penulisan harus
                                            +62</label>
                                        <input type="text" class="form-control" name="hp"
                                            value="{{ $is->hp }}">
                                    </div>
                                    <hr>
                                    <!-- Secondary Alert -->
                                    <div class="alert alert-secondary" role="alert">
                                        <strong> Kosongkan Form Dibawah ini, jika tidak ingin merubah datanya </strong>
                                    </div>
                                    <div>
                                        <label for="firstName" class="form-label">Username Login</label>
                                        <input type="text" class="form-control" name="username"
                                            value="{{ $is->username }}">
                                    </div>
                                    <div>
                                        <label for="image" class="form-label">Gambar</label>
                                        <input type="file" class="form-control" name="foto">
                                    </div>
                                    <div>
                                        <label for="firstName" class="form-label">Password Login</label>
                                        <input type="text" class="form-control" name="pwd">
                                    </div>
                                </div>
                                <!--end col-->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <!--end row-->
                    </form>
                </div>
            </div>
        </div>
        </div>
    @endforeach
    <!-- End Page-content -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>
                        document.write(new Date().getFullYear())
                    </script> © Nuskhu Digital.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
                        Design & Develop by Nuskhu Digital
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection
