@extends('template')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card rounded-0 bg-soft-success mx-n4 mt-n4 border-top">
                            <div class="px-4">
                                <div class="row">
                                    <div class="col-xxl-5 align-self-center">
                                        <div class="py-4">
                                            <h4 class="display-6 coming-soon-text">Selamat Datang
                                            </h4>
                                            <p class="text-success fs-15 mt-3">Halaman ini merupakan dashboard untuk segala
                                                kebutuhan anda
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 ms-auto">
                                        <div class="mb-n5 pb-1 faq-img d-none d-xxl-block">
                                            <img src="{{ 'assets/images/faq-img.png' }}" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <br>

                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card -->


                    </div>
                </div>
            </div>
            <!--end col-->.
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> © Nuskhu Digital.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            Design & Develop by Nuskhu Digital
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
