@extends('template')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0">Transaksi Masuk</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Ecommerce</a></li>
                                    <li class="breadcrumb-item active">Transaksi Masuk</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="card">
                        <div class="card-header border-0">
                            <form action="{{ route('pes.mask') }}" method="GET">
                                <div class="row g-4 mb-3">
                                    <div class="col-sm-auto">
                                        <div>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="d-flex justify-content-sm-end gap-2">
                                            <select class="form-control w-md" name="stat" data-choices
                                                data-choices-search-false>
                                                <option value="" disabled selected>All</option>
                                                <option value="on proses">On Proces</option>
                                                <option value="selesai">Selesai</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="d-flex justify-content-sm-end gap-2">
                                            <label for="startDate" class="form-label">Start Date:</label>
                                            <input type="date" class="form-control" name="startDate">
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="d-flex justify-content-sm-end gap-2">
                                            <label for="endDate" class="form-label">End Date:</label>
                                            <input type="date" class="form-control" name="endDate">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                        <div class="card-body">
                            <table id="buttons-datatables" class="display table table-bordered" style="width:100%">
                            {{-- <table class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%"> --}}
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 10px;">
                                            <div class="form-check">
                                                <input class="form-check-input fs-15" type="checkbox" id="checkAll"
                                                    value="option">
                                            </div>
                                        </th>
                                        <th>No</th>
                                        <th>User</th>
                                        <th>Invoice</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                        use Carbon\Carbon;
                                    @endphp
                                    @foreach ($ucart as $p)
                                        <tr>
                                            <th scope="row">
                                                <div class="form-check">
                                                    <input class="form-check-input fs-15" type="checkbox" name="checkAll"
                                                        value="option1">
                                                </div>
                                            </th>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $p->name }}</td>
                                            <td>{{ $p->invoice }}</td>
                                            <td>
                                                <?php if ($p->status == 'selesai') {
                                                    $n = 'success';
                                                } else {
                                                    $n = 'warning';
                                                } ?>
                                                <span
                                                    class="badge badge-gradient-{{ $n }}">{{ $p->status }}</span>
                                            </td>
                                            <td>{{ Carbon::parse($p->created_at)->locale('id')->isoFormat('dddd, D MMMM Y') }}
                                            </td>
                                            <td>Rp.{{ number_format($p->grandtotal) }}</td>
                                            <td>
                                                <a class="btn btn-ghost-info waves-effect waves-light btn-sm hapusBtn"
                                                    href="{{ route('detail.mask', ['id' => $p->invoice]) }}">
                                                    View
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end row -->

            </div>
            <!-- container-fluid -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
    </div>
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>
                        document.write(new Date().getFullYear())
                    </script> © Nuskhu Digital.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
                        Design & Develop by Nuskhu Digital
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection
