@extends('template')
@section('content')
    <div class="main-content">

    <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">Order Details</h4>
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Ecommerce</a></li>
                                        <li class="breadcrumb-item active">Order Details</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-xl-9">
                            <div class="card">
                                <div class="card-header">
                                    <div class="d-flex align-items-center">
                                        <h5 class="card-title flex-grow-1 mb-0">Order #{{ $ucart[0]->invoice }}</h5>
                                        <div class="flex-shrink-0 mt-2 mt-sm-0">
                                            @if ($ucart[0]->status=='selesai')
                                            <a  disabled class="btn btn-soft-success btn-sm mt-2 mt-sm-0"><i class="ri-checkbox-fill"></i> Telah Selesai</a>
                                            @else
                                            <a data-bs-toggle="modal" data-bs-target="#exampleModalgrid_{{ $ucart[0]->invoice }}" class="btn btn-soft-info btn-sm mt-2 mt-sm-0"><i class="ri-map-pin-line align-middle me-1"></i> Selesaikan</a>
                                            @endif

                                            {{-- <a href="javasccript:void(0;)" class="btn btn-soft-danger btn-sm mt-2 mt-sm-0"><i class="mdi mdi-archive-remove-outline align-middle me-1"></i> Reject</a> --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive table-card">
                                        <table class="table table-nowrap align-middle table-borderless mb-0">
                                            <thead class="table-light text-muted">
                                                <tr>
                                                    <th scope="col">Product Details</th>
                                                    <th scope="col">Item Price</th>
                                                    <th scope="col">Quantity</th>
                                                    <th scope="col" class="text-end">Total Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($isi as $i)
                                                <tr>
                                                    <td>
                                                        <div class="d-flex">
                                                            <div class="flex-shrink-0 avatar-md bg-light rounded p-1">
                                                                <img src="{{ asset('berkas/' . $i->file) }}" alt="" class="img-fluid d-block">
                                                            </div>
                                                            <div class="flex-grow-1 ms-3">
                                                                <h5 class="fs-15"><a href="apps-ecommerce-product-details.html" class="link-primary">{{ $i->name }}</a></h5>
                                                                <p class="text-muted mb-0">Ukuran : <span class="fw-medium">{{ $i->ukuran }}</span></p>
                                                                {{-- <p class="text-muted mb-0">Size: <span class="fw-medium">M</span></p> --}}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <?php if ($i->range==1) {?>
                                                        <td>Rp.{{ number_format($i->hargaSatuan) }}</td>
                                                        <?php }else{?>
                                                            <td>Rp.{{ number_format($i->hargaprod) }}</td>
                                                    <?php }?>
                                                    <td>{{ $i->jumlah }}</td>
                                                    <td class="fw-medium text-end">
                                                        Rp.{{ number_format($i->total) }}
                                                    </td>
                                                </tr>
                                                @endforeach

                                                <tr class="border-top border-top-dashed">
                                                    <td colspan="3"></td>
                                                    <td colspan="2" class="fw-medium p-0">
                                                        <table class="table table-borderless mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Sub Total :</td>
                                                                    <td class="text-end">Rp.{{ number_format($ucart[0]->jumlah) }}</td>
                                                                </tr>
                                                                {{-- <tr>
                                                                    <td>Discount <span class="text-muted">(VELZON15)</span> : :</td>
                                                                    <td class="text-end">-$53.99</td>
                                                                </tr> --}}
                                                                <tr>
                                                                    <td>Shipping Charge :</td>
                                                                    <td class="text-end">Rp.{{ number_format($ucart[0]->ongkir) }}</td>
                                                                </tr>
                                                                {{-- <tr>
                                                                    <td>Estimated Tax :</td>
                                                                    <td class="text-end">$44.99</td>
                                                                </tr> --}}
                                                                <tr class="border-top border-top-dashed">
                                                                    <th scope="row">Grand Total :</th>
                                                                    <th class="text-end">Rp.{{ number_format($ucart[0]->grandtotal) }}</th>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--end card-->
                            <div class="card">
                                <div class="card-header">
                                    <div class="d-sm-flex align-items-center">
                                        <h5 class="card-title flex-grow-1 mb-0">Bukti Pengiriman</h5>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="profile-timeline">
                                        <div class="accordion accordion-flush" id="accordionFlushExample">
                                            <div class="accordion-item border-0">
                                                <div class="accordion-header" id="headingOne">
                                                    <a class="accordion-button p-2 shadow-none" data-bs-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        <div class="d-flex align-items-center">
                                                            <div class="flex-shrink-0 avatar-xs">
                                                                <div class="avatar-title bg-success rounded-circle">
                                                                    <i class="ri-shopping-bag-line"></i>
                                                                </div>
                                                            </div>
                                                            @php
                                                                  use Carbon\Carbon;
                                                            @endphp
                                                            @if ($ucart[0]->buktikirim)
                                                            <div class="flex-grow-1 ms-3">
                                                                <h6 class="fs-15 mb-0 fw-semibold">File Bukti Pengiriman - <span class="fw-normal">{{ Carbon::parse($ucart[0]->updated_at)->locale('id')->isoFormat('dddd, D MMMM Y') }}</span></h6>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </a>
                                                </div>
                                                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body ms-2 ps-5 pt-0">
                                                        <div class="element-item col-xxl-3 col-xl-4 col-sm-6 project designing development" data-category="designing development">
                                                            <div class="gallery-box card">
                                                                <div class="gallery-container">
                                                                    <a class="image-popup" href="{{ asset('berkas/buktikirim/' . $ucart[0]->buktikirim) }}" title="">
                                                                        <img class="gallery-img img-fluid mx-auto" src="{{ asset('berkas/buktikirim/' . $ucart[0]->buktikirim) }}" alt="" />
                                                                    </a>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!--end accordion-->
                                    </div>
                                </div>
                            </div>
                            <!--end card-->
                        </div>
                        <!--end col-->
                        <div class="col-xl-3">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title mb-0"><i class="ri-map-pin-line align-middle me-1 text-muted"></i> Billing Address</h5>
                                </div>
                                <div class="card-body">
                                    <ul class="list-unstyled vstack gap-2 fs-13 mb-0">
                                        <li class="fw-medium fs-14">{{ $ucart[0]->name }}</li>
                                        <li>{{ $ucart[0]->nomorhp }}</li>
                                        <li>{{ $ucart[0]->email }}</li>
                                        <li>{{ $ucart[0]->alamat }}</li>
                                        <li>{{ $ucart[0]->n_kota }}, {{ $ucart[0]->n_kec }} </li>
                                        <li>{{ $ucart[0]->n_prov }}</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <div class="d-flex">
                                        <h5 class="card-title flex-grow-1 mb-0"><i class="mdi mdi-truck-fast-outline align-middle me-1 text-muted"></i> Logistics Details</h5>
                                        {{-- <div class="flex-shrink-0">
                                            <a href="javascript:void(0);" class="badge badge-soft-primary fs-11">Track Order</a>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="text-center">
                                        <lord-icon src="https://cdn.lordicon.com/uetqnvvg.json" trigger="loop" colors="primary:#405189,secondary:#0ab39c" style="width:80px;height:80px"></lord-icon>
                                        <h5 class="fs-16 mt-2">{{ $ucart[0]->kurir }}</h5>
                                        <p class="text-muted mb-0">{{ $ucart[0]->kurir_paket }}</p>
                                        <p class="text-muted mb-0">Biaya : {{ $ucart[0]->ongkir }}</p>
                                    </div>
                                </div>
                            </div>
                            <!--end card-->

                            <!--end card-->
                            {{-- <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title mb-0"><i class="ri-map-pin-line align-middle me-1 text-muted"></i> Shipping Address</h5>
                                </div>
                                <div class="card-body">
                                    <ul class="list-unstyled vstack gap-2 fs-13 mb-0">
                                        <li class="fw-medium fs-14">Joseph Parker</li>
                                        <li>+(256) 245451 451</li>
                                        <li>2186 Joyce Street Rocky Mount</li>
                                        <li>California - 24567</li>
                                        <li>United States</li>
                                    </ul>
                                </div>
                            </div> --}}
                            <!--end card-->

                            {{-- <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title mb-0"><i class="ri-secure-payment-line align-bottom me-1 text-muted"></i> Payment Details</h5>
                                </div>
                                <div class="card-body">
                                    <div class="d-flex align-items-center mb-2">
                                        <div class="flex-shrink-0">
                                            <p class="text-muted mb-0">Transactions:</p>
                                        </div>
                                        <div class="flex-grow-1 ms-2">
                                            <h6 class="mb-0">#VLZ124561278124</h6>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center mb-2">
                                        <div class="flex-shrink-0">
                                            <p class="text-muted mb-0">Payment Method:</p>
                                        </div>
                                        <div class="flex-grow-1 ms-2">
                                            <h6 class="mb-0">Debit Card</h6>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center mb-2">
                                        <div class="flex-shrink-0">
                                            <p class="text-muted mb-0">Card Holder Name:</p>
                                        </div>
                                        <div class="flex-grow-1 ms-2">
                                            <h6 class="mb-0">Joseph Parker</h6>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center mb-2">
                                        <div class="flex-shrink-0">
                                            <p class="text-muted mb-0">Card Number:</p>
                                        </div>
                                        <div class="flex-grow-1 ms-2">
                                            <h6 class="mb-0">xxxx xxxx xxxx 2456</h6>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <div class="flex-shrink-0">
                                            <p class="text-muted mb-0">Total Amount:</p>
                                        </div>
                                        <div class="flex-grow-1 ms-2">
                                            <h6 class="mb-0">$415.96</h6>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <!--end card-->
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->

                </div><!-- container-fluid -->
            </div><!-- End Page-content -->

        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
    <div class="modal fade" id="exampleModalgrid_{{ $ucart[0]->invoice }}" tabindex="-1"
        aria-labelledby="exampleModalgridLabel" aria-modal="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{ route('tuka') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-lg-12">
                            <div class="col-xxl-12">
                                <div>
                                    <label for="firstName" class="form-label">Upload Bukti Pengiriman</label>
                                    <input type="file" class="form-control" name="bukti">
                                    <input type="hidden" class="form-control" name="idseo"
                                        value="{{ $ucart[0]->invoice }}">
                                </div>

                            </div>
                            <!--end col-->
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!--end row-->
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>
                        document.write(new Date().getFullYear())
                    </script> © Nuskhu Digital.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
                        Design & Develop by Nuskhu Digital
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection
