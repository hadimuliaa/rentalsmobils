<div class="app-menu navbar-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box">
        <!-- Dark Logo-->
        <a href="index.html" class="logo logo-dark">
            <span class="logo-sm">
                <img src="{{ asset('assetsfe/images/lo.png') }}" alt="" height="100">
            </span>
            <span class="logo-lg">
                <img src="{{ asset('assetsfe/images/lo.png') }}" alt="" height="100">
            </span>
        </a>
        <!-- Light Logo-->
        <a href="index.html" class="logo logo-light">
            <span class="logo-sm">
                <img src="{{ asset('assetsfe/images/lo.png') }}" alt="" height="50">
            </span>
            <span class="logo-lg">
                <img src="{{ asset('assetsfe/images/lo.png') }}" alt="" height="100">
            </span>
        </a>
        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover"
            id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">

            <div id="two-column-menu">
            </div>
            <ul class="navbar-nav" id="navbar-nav">
                <li class="menu-title"><span data-key="t-menu">Menu</span></li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="{{ url('/utamin') }}" role="button" aria-expanded="false"
                        aria-controls="sidebarDashboards">
                        <i class="ri-dashboard-2-line"></i> <span data-key="t-dashboards">Dashboards</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->is('pelanggan*') || request()->is('admin*') ? 'active' : '' }}"
                        href="{{ route('pelanggan') }}" role="button" aria-expanded="false"
                        aria-controls="sidebarDashboards">
                        <i class="ri-file-text-fill"></i> <span data-key="t-dashboards">Pengaturan Akun</span>
                    </a>
                </li>


                {{-- ----produk --}}
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->is('categori*') || request()->is('testi*')||request()->is('knowledge*') || request()->is('hero*') || request()->is('product*') ? 'active' : '' }}"
                        href="#cat" data-bs-toggle="collapse" role="button" aria-expanded="false"
                        aria-controls="categori">
                        <i class="ri-file-text-fill"></i> <span data-key="t-dashboards">Management Mobil</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->is('product*')|| request()->is('pengembalian*') ? 'show' : '' }}"
                        id="cat">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('product*') ? 'active' : '' }}"
                                    href="{{ route('product') }}" role="button" aria-expanded="false"
                                    aria-controls="cat">
                                    <i class="ri-file-text-fill"></i> <span data-key="t-dashboards">Tambah Mobil</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('pengembalian*') ? 'active' : '' }}"
                                    href="{{ route('kembalian') }}" role="button" aria-expanded="false"
                                    aria-controls="cat">
                                    <i class="ri-file-text-fill"></i> <span data-key="t-dashboards">Pengembalian</span>
                                </a>
                            </li>
                        </ul>


                    </div>
                </li> <!-- end jjone Menu -->


                {{-- ----produk --}}
                @if (Auth::check() && Auth::user()->role === 'user')

                @endif

                @if (Auth::check() && Auth::user()->role === 'admin')
                    <!-- end Dashboard Menu -->
                @endif

            </ul>
        </div>
        <!-- Sidebar -->
    </div>

    <div class="sidebar-background"></div>
</div>
