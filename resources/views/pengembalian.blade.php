@extends('template')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0">Pengembalian</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Pengembalian</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <div class="row">
                    <div class="col">
                        <div class="h-100">
                            <div class="row mb-3 pb-1">
                                <div class="col-12">
                                    <form action="{{ route('kembalian') }}" method="get">
                                        <div class="row g-3 mb-0 align-items-center">
                                            <div class="col-"> <!-- Menggunakan col- tanpa nilai lebar -->
                                                <div class="input-group">
                                                    <input type="text" class="form-control border-0" name="plat"
                                                        placeholder="Masukkan Plat Kendaraan Yang Anda Sewa">
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <!-- Menggunakan col-auto agar tombol tetap pas di sampingnya -->
                                                <button type="submit" class="btn btn-soft-success">
                                                    <i class="ri-add-circle-line align-middle me-1"></i> Cari
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!--end col-->
                            </div>
                            <!--end row-->
                        </div> <!-- end .h-100-->

                    </div>
                </div>
                <div class="row">
                    <div class="card">

                        <div class="card-body">
                            <table class="table table-bordered dt-responsive nowrap table-striped align-middle"
                                style="width:100%">
                                <thead>
                                    <tr>

                                        <th>No</th>
                                        <th>Merek</th>
                                        <th>Model</th>
                                        <th>Plat</th>
                                        <th>Tanggal Sewa </th>
                                        <th>Harga Sewa/Hari</th>
                                        <th>Total Biaya</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($product as $p)
                                        <tr>

                                            <td>{{ $no++ }} </td>
                                            <td>{{ $p->merek }} </td>
                                            <td>{{ $p->model }}</td>
                                            <td>{{ $p->plat }}</td>
                                            <td>
                                                {{ date('d M Y', strtotime($p->mulai)) }} to
                                                {{ date('d M Y', strtotime($p->akhir)) }} </td>
                                            <td>{{ number_format($p->sewa, 0, ',', '.') }}</td>
                                            <td>{{ number_format($p->totalbiaya, 0, ',', '.') }}</td>
                                            <td>
                                                <?php if ($p->status=='menunggu') { ?>
                                                    <a type="button" class="btn btn-warning waves-effect waves-light">Menunggu Persetujuan</a>
                                                <?php }else{ ?>
                                                <a data-bs-toggle="modal" data-bs-target="#sewa_{{ $p->idts }}"
                                                    type="button" class="btn btn-info waves-effect waves-light">Kembalikan
                                                    Sekarang</a>
                                                <?php } ?>
                                            </td>

                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end row -->

            </div>
            <!-- container-fluid -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
    @foreach ($product as $is)
        <div class="modal fade" id="sewa_{{ $is->idts }}" tabindex="-1" aria-labelledby="exampleModalgridLabel"
            aria-modal="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <form action="{{ route('baliakan') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="col-lg-12">
                                <div class="col-xxl-12">
                                    Silahkan Lakukan Pembayaran Kepada Rekening :
                                    <br>
                                    BCA : 7285602068
                                    <br>
                                    a.n Hadi Mulia
                                    <input type="hidden" class="form-control" name="idtrx" value="{{ $is->idts }}">
                                    <input type="hidden" class="form-control" name="iduser" value="{{ $userid }}">
                                    <input type="hidden" class="form-control" name="totalbiaya"
                                        value="{{ $is->totalbiaya }}">

                                    <div>
                                        <label for="s" class="form-label">Upload Bukti</label>
                                        <input name="bukti" type="file" class="form-control" id="s">
                                    </div>
                                </div>
                                <!--end col-->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Ya, Sudah Saya Transfer</button>
                            </div>
                    </div>
                    <!--end row-->
                    </form>
                </div>
            </div>
        </div>
        </div>
    @endforeach


    </div>
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>
                        document.write(new Date().getFullYear())
                    </script> © Nuskhu Digital.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
                        Design & Develop by Nuskhu Digital
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection
