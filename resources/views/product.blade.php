@extends('template')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0">Products</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Products</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <div class="row">
                    <div class="col">
                        <div class="h-100">
                            <div class="row mb-3 pb-1">
                                <div class="col-12">
                                    <div class="d-flex align-items-lg-center flex-lg-row flex-column">
                                        <div class="flex-grow-1">
                                            <a data-bs-toggle="modal" data-bs-target="#addmobil"  class="btn btn-success" id="addproduct-btn"><i
                                                    class="ri-add-line align-bottom me-1"></i> Add
                                                Mobil</a>
                                        </div>
                                        <div class="flex-grow-1">
                                            <a data-bs-toggle="modal" data-bs-target="#newbooks" type="button"
                                                class="btn btn-primary waves-effect waves-light">New Booking</a>
                                        </div>
                                        <div class="mt-3 mt-lg-0">
                                            <form action="{{ route('product') }}" method="get">
                                                <div class="row g-3 mb-0 align-items-center">
                                                    <div class="col-sm-auto">
                                                        <div class="input-group">
                                                            <input type="text"
                                                                class="form-control border-0 dash-filter-picker shadow"
                                                                data-provider="flatpickr" data-range-date="true"
                                                                data-date-format="d M, Y"
                                                                data-deafult-date="01 Jan 2022 to 31 Jan 2022"
                                                                name="daterange" placeholder="Pilih rentang tanggal">
                                                            <div
                                                                class="input-group-text bg-primary border-primary text-white">
                                                                <i class="ri-calendar-2-line"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                    <div class="col-sm-auto">
                                                        <select class="form-select" name="merek" aria-label="Merek">
                                                            <option selected disabled>Pilih Merek</option>
                                                            @foreach ($merk as $m)
                                                                <option value="{{ $m->merek }}">{{ $m->merek }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <!--end col-->
                                                    <div class="col-sm-auto">
                                                        <select class="form-select" name="model" aria-label="Model">
                                                            <option selected disabled>Pilih Model</option>
                                                            @foreach ($modl as $m)
                                                                <option value="{{ $m->model }}">{{ $m->model }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <!--end col-->
                                                    <div class="col-auto">
                                                        <button type="submit" class="btn btn-soft-success">
                                                            <i class="ri-add-circle-line align-middle me-1"></i> Cari
                                                        </button>
                                                    </div>
                                                    <!--end col-->
                                                </div>
                                                <!--end row-->
                                            </form>

                                        </div>
                                    </div><!-- end card header -->
                                </div>
                                <!--end col-->
                            </div>
                            <!--end row-->
                        </div> <!-- end .h-100-->

                    </div>
                </div>
                <div class="row">
                    <div class="card">

                        <div class="card-body">
                            <table class="table table-bordered dt-responsive nowrap table-striped align-middle"
                                style="width:100%">
                                <thead>
                                    <tr>

                                        <th>No</th>
                                        <th>Merek</th>
                                        <th>Model</th>
                                        <th>Plat</th>
                                        <th>Tanggal Sewa </th>
                                        <th>Harga Sewa</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($product as $p)
                                        <tr>

                                            <td>{{ $no++ }} </td>
                                            <td>{{ $p->merek }} </td>
                                            <td>{{ $p->model }}</td>
                                            <td>{{ $p->plat }}</td>
                                            <td>
                                                <?php if ($p->stat!=0) {?>
                                                {{ date('d M Y', strtotime($p->mulai)) }} to
                                                {{ date('d M Y', strtotime($p->akhir)) }} </td>
                                            <?php } ?>

                                            <td>{{ number_format($p->sewa, 0, ',', '.') }}</td>
                                            <td>
                                                <?php if ($p->iduser==$userid) { ?>
                                                <div class="dropdown d-inline-block">
                                                    <button class="btn btn-soft-secondary btn-sm dropdown" type="button"
                                                        data-bs-toggle="dropdown" aria-expanded="false">
                                                        <i class="ri-more-fill align-middle"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-end">
                                                        <li><a href="#!" class="dropdown-item"><i
                                                                    class="ri-eye-fill align-bottom me-2 text-muted"></i>
                                                                View</a></li>
                                                        <li>
                                                            <a data-bs-toggle="modal"
                                                                data-bs-target="#exampleModalgrid_{{ $p->plat }}"
                                                                class="dropdown-item edit-item-btn"><i
                                                                    class="ri-pencil-fill align-bottom me-2 text-muted"></i>
                                                                Edit</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ route('delete-product', ['id' => $p->plat]) }}"
                                                                class="dropdown-item remove-item-btn">
                                                                <i
                                                                    class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>
                                                                Delete
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                {{-- swa --}}
                                                <?php } ?>
                                                <?php if ($p->stat==0 && ($p->iduser!=$userid)) {?>
                                                <a data-bs-toggle="modal" data-bs-target="#sewa_{{ $p->plat }}"
                                                    type="button" class="btn btn-info waves-effect waves-light">Booking
                                                    Now</a>
                                                <?php }else if ($p->stat!=0 ){?>
                                                <a type="button"
                                                    class="btn btn-warning waves-effect waves-light">Booked</a>
                                                <?php } ?>
                                            </td>

                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end row -->

            </div>
            <!-- container-fluid -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
    @foreach ($product as $is)
        <div class="modal fade" id="exampleModalgrid_{{ $is->plat }}" tabindex="-1"
            aria-labelledby="exampleModalgridLabel" aria-modal="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <form action="{{ route('uprod') }}" method="POST">
                            @csrf
                            <div class="col-lg-12">
                                <div class="col-xxl-12">
                                    <div>
                                        <label for="firstName" class="form-label">Merek</label>
                                        <input type="text" class="form-control" name="title"
                                            value="{{ $is->merek }}">
                                        <input type="hidden" class="form-control" name="idmobil"
                                            value="{{ $is->plat }}">
                                    </div>

                                    <div>
                                        <label for="model" class="form-label">Model</label>
                                        <input type="text" class="form-control" name="model"
                                            value="{{ $is->model }}">
                                    </div>

                                    <div>
                                        <label for="plat" class="form-label">Plat</label>
                                        <input type="text" class="form-control" name="plat"
                                            value="{{ $is->plat }}">
                                    </div>

                                    <div>
                                        <label for="sewa" class="form-label">Sewa</label>
                                        <input type="text" class="form-control" name="sewa"
                                            value="{{ $is->sewa }}">
                                    </div>


                                </div>
                                <!--end col-->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </div>
                    <!--end row-->
                    </form>
                </div>
            </div>
        </div>
        </div>
    @endforeach

    @foreach ($product as $is)
        <div class="modal fade" id="sewa_{{ $is->plat }}" tabindex="-1" aria-labelledby="exampleModalgridLabel"
            aria-modal="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <form action="{{ route('uprodSewa') }}" method="POST">
                            @csrf
                            <div class="col-lg-12">
                                <div class="col-xxl-12">
                                    <input type="hidden" class="form-control" name="idmobil"
                                        value="{{ $is->plat }}">
                                    <input type="hidden" class="form-control" name="iduser"
                                        value="{{ $userid }}">
                                    <div>
                                        <label for="exampleInputdate" class="form-label">Tanggal Mulai</label>
                                        <input name="mulai" type="date" class="form-control" id="exampleInputdate">
                                    </div>
                                    <div>
                                        <label for="s" class="form-label">Tanggal Selesai</label>
                                        <input name="akhir" type="date" class="form-control" id="s">
                                    </div>
                                </div>
                                <!--end col-->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </div>
                    <!--end row-->
                    </form>
                </div>
            </div>
        </div>
        </div>
    @endforeach
    <div class="modal fade" id="newbooks" tabindex="-1" aria-labelledby="exampleModalgridLabel" aria-modal="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{ route('uprodSewa') }}" method="POST">
                        @csrf
                        <div class="col-lg-12">
                            <div class="col-xxl-12">
                                <select class="form-select" name="idmobil" aria-label="Model">
                                    <option selected disabled>Pilih Model dan Merek</option>
                                    @foreach ($mobls as $m)
                                        <option value="{{ $m->plat }}">{{ $m->model }} - {{ $m->merek }}
                                        </option>
                                    @endforeach
                                </select>

                                <input type="hidden" class="form-control" name="iduser" value="{{ $userid }}">
                                <div>
                                    <label for="exampleInputdate" class="form-label">Tanggal Mulai</label>
                                    <input name="mulai" type="date" class="form-control" id="exampleInputdate">
                                </div>
                                <div>
                                    <label for="s" class="form-label">Tanggal Selesai</label>
                                    <input name="akhir" type="date" class="form-control" id="s">
                                </div>
                            </div>
                            <!--end col-->
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                </div>
                <!--end row-->
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addmobil" tabindex="-1" aria-labelledby="exampleModalgridLabel" aria-modal="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{ route('admobilss') }}" method="POST">
                        @csrf
                        <div class="col-lg-12">
                            <div class="col-xxl-12">
                                <div>
                                    <label for="firstName" class="form-label">Merek</label>
                                    <input type="text" class="form-control" name="title">
                                    <input type="hidden" class="form-control" name="iduser"
                                        value="{{ $userid }}">
                                </div>

                                <div>
                                    <label for="model" class="form-label">Model</label>
                                    <input type="text" class="form-control" name="model">
                                </div>

                                <div>
                                    <label for="plat" class="form-label">Plat</label>
                                    <input type="text" class="form-control" name="plat">
                                </div>

                                <div>
                                    <label for="sewa" class="form-label">Sewa</label>
                                    <input type="text" class="form-control" name="sewa">
                                </div>


                            </div>
                            <!--end col-->
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                </div>
                <!--end row-->
                </form>
            </div>
        </div>
    </div>
    </div>
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>
                        document.write(new Date().getFullYear())
                    </script> © Nuskhu Digital.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
                        Design & Develop by Nuskhu Digital
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection
