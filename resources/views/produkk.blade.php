@extends('template')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0">
                                @if(isset($id))
                                    Testimoni
                                @else
                                    Products
                                @endif
                            </h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Ecommerce</a></li>
                                    <li class="breadcrumb-item active">
                                        @if(isset($id))
                                            Testimoni
                                        @else
                                            Products
                                        @endif
                                    </li>
                                </ol>
                            </div>

                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="card">
                        <div class="card-body">
                            <div class="col-xl-12">
                                <div class="card">
                                    @if ($id)
                                    <form class="step1Form" action="{{ route('simpan-product') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <input type="hidden" class="form-control" value="testi" name="name">
                                            {{-- <input type="hidden" value="0" class="form-control" name="harga">
                                            <input type="hidden" value="0" class="form-control" name="category_id">
                                            <input type="hidden" value="0" class="form-control" name="ukuran">
                                            <input type="hidden" value="0" class="form-control" name="berat"> --}}
                                            <input type="hidden" value="{{ $id === 'testi' ? 2 : ($id === 'hero' ? 3 : 1) }}"
                                            class="form-control" name="tipe">
                                            <div class="col-12">
                                                <div class="mb-3">
                                                    <div class="form-floating">
                                                        <textarea class="form-control" name="desc"></textarea>
                                                        <label for="firstnamefloatingInput">Deskripsi</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-3">
                                                    <div class="form-floating">
                                                        <input type="file" class="form-control" name="foto">
                                                        <label for="firstnamefloatingInput">Foto</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>

                                        <!-- Buttons Grid -->
                                        <div class="d-grid gap-2">
                                            <button class="btn btn-primary" type="submit">Simpan</button>
                                        </div>
                                    </form>
                                    @else
                                    <form class="step1Form" action="{{ route('simpan-product') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-3">
                                                    <div class="form-floating">
                                                        <input type="text" class="form-control" name="name">
                                                        <label for="firstnamefloatingInput">Nama Produk</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end col-->
                                            <div class="col-12">
                                                <div class="mb-3">
                                                    <div class="form-floating">
                                                        <input type="number" class="form-control" name="harga">
                                                        <label for="firstnamefloatingInput">Harga</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-3">
                                                    <label for="category">Kategori Produk</label>
                                                    <select class="form-select" name="category_id" id="category">
                                                        @foreach($categories as $category)
                                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-3">
                                                    <div class="form-floating">
                                                        <textarea class="form-control" name="desc"></textarea>
                                                        <label for="firstnamefloatingInput">Deskripsi</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-3">
                                                    <div class="form-floating">
                                                        <input type="text" class="form-control" name="ukuran">
                                                        <label for="firstnamefloatingInput">Ukuran</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-3">
                                                    <div class="form-floating">
                                                        <input type="text" class="form-control" name="berat">
                                                        <label for="firstnamefloatingInput">Berat (dalam ukuran Gram)</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-3">
                                                    <div class="form-floating">
                                                        <input type="file" class="form-control" name="foto">
                                                        <label for="firstnamefloatingInput">Foto</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>

                                        <!-- Buttons Grid -->
                                        <div class="d-grid gap-2">
                                            <button class="btn btn-primary" type="submit">Simpan</button>
                                        </div>
                                    </form>
                                    @endif
                                    <!-- end card body -->
                                </div>
                                <!-- end card -->
                            </div>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end row -->

            </div>
            <!-- container-fluid -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>
                        document.write(new Date().getFullYear())
                    </script> © Nuskhu Digital.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
                        Design & Develop by Nuskhu Digital
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection
