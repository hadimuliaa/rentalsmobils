@extends('template')
@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">

                <div class="position-relative mx-n4 mt-n4">
                    <div class="profile-wid-bg profile-setting-img">
                        <img src="assets/images/profile-bg.jpg" class="profile-wid-img" alt="">
                        <div class="overlay-content">
                            <div class="text-end p-3">
                                <div class="p-0 ms-auto rounded-circle profile-photo-edit">
                                    <input id="profile-foreground-img-file-input" type="file"
                                        class="profile-foreground-img-file-input">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xxl-3">
                        <div class="card mt-n5">
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <div class="profile-user position-relative d-inline-block mx-auto  mb-4">
                                        <img src="assets/images/users/avatar-1.jpg"
                                            class="rounded-circle avatar-xl img-thumbnail user-profile-image"
                                            alt="user-profile-image">
                                        <div class="avatar-xs p-0 rounded-circle profile-photo-edit">
                                            <input id="profile-img-file-input" type="file"
                                                class="profile-img-file-input">
                                            <label for="profile-img-file-input" class="profile-photo-edit avatar-xs">
                                                <span class="avatar-title rounded-circle bg-light text-body">
                                                    <i class="ri-camera-fill"></i>
                                                </span>
                                            </label>
                                        </div>
                                    </div>

                                    <h5 class="fs-16 mb-1">
                                        {{ $cred->name }}
                                    </h5>
                                    <p class="text-muted mb-0">{{ $cred->email }}</p>
                                </div>
                            </div>
                        </div>
                        <!--end card-->
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex align-items-center mb-4">
                                    <div class="flex-grow-1">
                                        <h5 class="card-title mb-0">Riwayat Pengajuan</h5>
                                    </div>

                                </div>
                                <?php
                                foreach ($history as $h) {?>
                                <div class="mb-3">
                                    <div class="list-group list-group-fill-success">
                                        <a href="{{ route('showbarusan', ['id' => $h->idd]) }}"
                                            class="list-group-item list-group-item-action"><i
                                                class="{{ $h->logo }}"></i>Jenis Pengajuan
                                            : <br>{{ $h->jenis }}
                                            <br>
                                            Diajukan Pada : {{ date('d F Y', strtotime($h->tgl)) }}</a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->
                    <div class="col-xxl-9">
                        <div class="card mt-xxl-n5">
                            <div class="card-header">
                                <ul class="nav nav-tabs-custom rounded card-header-tabs border-bottom-0" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-bs-toggle="tab" href="#personalDetails"
                                            role="tab">
                                            <i class="fas fa-home"></i>Personal
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-bs-toggle="tab" href="#pangkat" role="tab">
                                            <i class="far fa-user"></i>Pangkat
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-bs-toggle="tab" href="#jabatan" role="tab">
                                            <i class="far fa-envelope"></i>Jabatan
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-bs-toggle="tab" href="#pendidikan" role="tab">
                                            <i class="far fa-envelope"></i>Pendidikan
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-bs-toggle="tab" href="#diklat" role="tab">
                                            <i class="far fa-envelope"></i>Diklat
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-bs-toggle="tab" href="#pak" role="tab">
                                            <i class="far fa-envelope"></i>PAK
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body p-4">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="personalDetails" role="tabpanel">
                                        <form action="{{ route('act_personal') }}" method="POST">
                                            <div class="row">
                                                @csrf
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="firstnameInput" class="form-label">NIP</label>
                                                        <input name="nip" type="text" class="form-control"
                                                            value="{{ $cred->nip }}">
                                                        <input type="hidden" name="user_id" class="form-control"
                                                            value="{{ $cred->id }}">
                                                    </div>
                                                </div>
                                                <!--end col-->
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="lastnameInput" class="form-label">Nama</label>
                                                        <input name="name" type="text" class="form-control"
                                                            value="{{ $cred->name }}">
                                                    </div>
                                                </div>
                                                <!--end col-->
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="emailInput" class="form-label">Email</label>
                                                        <input type="email" class="form-control" name="email"
                                                            placeholder="Enter your email" value="{{ $cred->email }}">
                                                    </div>
                                                </div>
                                                <!--end col-->
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="addressInput" class="form-label">Alamat</label>
                                                        <input type="text" name="alamat" class="form-control"
                                                            value="{{ $cred->alamat }}">
                                                    </div>
                                                </div>
                                                <!--end col-->
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="phonenumberInput" class="form-label">Nomor
                                                            HP</label>
                                                        <input type="text" class="form-control" name="nomorhp"
                                                            value="{{ $cred->nomorhp }}">
                                                    </div>
                                                </div>
                                                <!--end col-->
                                                <div class="col-lg-12">
                                                    <div class="hstack gap-2 justify-content-end">
                                                        <button type="submit" class="btn btn-primary">Updates</button>
                                                    </div>
                                                </div>
                                                <!--end col-->
                                            </div>
                                            <!--end row-->
                                        </form>
                                    </div>
                                    <!--end tab-pane-->
                                    <div class="tab-pane" id="pangkat" role="tabpanel">
                                        <div class="mt-4 mb-3 border-bottom pb-2">
                                            <div class="float-end">
                                                <button type="button"
                                                    class="btn btn-ghost-dark waves-effect waves-light btn-sm"
                                                    data-bs-toggle="modal" data-bs-target="#myModal">Tambah</button>
                                                <!-- Default Modals -->
                                                <div id="myModal" class="modal fade" tabindex="-1"
                                                    aria-labelledby="myModalLabel" aria-hidden="true"
                                                    style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel">Pangkat Baru
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"> </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ route('save_pangkat') }}"
                                                                    method="POST">
                                                                    <div class="row g-3">
                                                                        @csrf
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Nomor SK</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="nomorsk">
                                                                                <input type="hidden" name="id"
                                                                                    value="{{ $hd }}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xxl-6">
                                                                            <div>
                                                                                <label for="ForminputState"
                                                                                    class="form-label">Pangkat</label>
                                                                                <select name="cat" id="prod_cat_id"
                                                                                    class="form-select productcategory">
                                                                                    <option value="0" disabled="true"
                                                                                        selected="true">-Select-
                                                                                    </option>
                                                                                    @foreach ($cat as $c)
                                                                                        <option
                                                                                            value="{{ $c->id }}">
                                                                                            {{ $c->nama }}
                                                                                        </option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                        <div class="col-xxl-6">
                                                                            <div>
                                                                                <label class="form-label">Golongan</label>
                                                                                <select name="subcat"
                                                                                    id="second_dropdown"
                                                                                    class="form-select">
                                                                                    <option value="0" disabled="true"
                                                                                        selected="true">-Select-
                                                                                    </option>
                                                                                    @foreach ($subc as $ks)
                                                                                        <option
                                                                                            value="{{ $ks->id }}">
                                                                                            {{ $ks->nama }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                        <div class="col-xxl-6">
                                                                            <div>
                                                                                <label class="form-label">Tanggal
                                                                                    SK</label>
                                                                                <input type="date" class="form-control"
                                                                                    name="tanggalsk">
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                        <div class="col-xxl-6">
                                                                            <div>
                                                                                <label class="form-label">TMT
                                                                                    Pangkat</label>
                                                                                <input type="text" name="tmtpangkat"
                                                                                    class="form-control">
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                        <div class="col-lg-12">
                                                                            <div class="hstack gap-2 justify-content-end">
                                                                                <button type="button"
                                                                                    class="btn btn-light"
                                                                                    data-bs-dismiss="modal">Close</button>
                                                                                <button type="submit"
                                                                                    class="btn btn-primary">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                    </div>
                                                                    <!--end row-->
                                                                </form>
                                                            </div>

                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                            <h5 class="card-title">Pangkat History</h5>
                                        </div>
                                        <?php foreach ($UserPangkat as $p) {?>
                                        <div class="d-flex align-items-center mb-3">
                                            <div class="flex-shrink-0 avatar-sm">
                                                <div class="avatar-title bg-light text-primary rounded-3 fs-18">
                                                    <i class="ri-medal-fill"></i>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1 ms-3">
                                                <h6>{{ $p->nampang }} / {{ $p->namgol }}</h6>
                                                <p class="text-muted mb-0">No.SK : {{ $p->nomorsk }}
                                                    <br> TMT.Pangkat : {{ $p->tmtpangkat }}
                                                </p>
                                            </div>
                                            <div>
                                                <button type="button"
                                                    class="btn btn-ghost-dark waves-effect waves-light btn-sm"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#id_{{ $p->id }}">Edit</button>

                                                <a href="{{ route('hapus_pangkat', ['id' => $p->id, 'userId' => $hd]) }}"
                                                    type="button"
                                                    class="btn btn-ghost-danger waves-effect waves-light btn-sm">Delete</a>


                                                <div id="id_{{ $p->id }}" class="modal fade" tabindex="-1"
                                                    aria-labelledby="myModalLabel" aria-hidden="true"
                                                    style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel">Editt Pangkat
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"> </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ route('update_pangkat') }}"
                                                                    method="POST">
                                                                    <div class="row g-3">
                                                                        @csrf
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Nomor SK</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="nomorsk"
                                                                                    value="{{ $p->nomorsk }}">
                                                                                <input type="hidden" name="id"
                                                                                    value="{{ $p->id }}">
                                                                                <input type="hidden" name="hd"
                                                                                    value="{{ $hd }}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xxl-6">
                                                                            <div>
                                                                                <label for="ForminputState"
                                                                                    class="form-label">Pangkat</label>
                                                                                <select name="cat" id="prod_cat_id"
                                                                                    class="form-select productcategory">
                                                                                    <option value="0" disabled="true"
                                                                                        selected="true">-Select-
                                                                                    </option>
                                                                                    @foreach ($cat as $ccc)
                                                                                        <option
                                                                                            value="{{ $ccc->id }}"
                                                                                            @selected($p->pangkat == $ccc->id)>
                                                                                            {{ $ccc->nama }}
                                                                                        </option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                        <div class="col-xxl-6">
                                                                            <div>
                                                                                <label class="form-label">Golongan</label>
                                                                                <select name="subcat"
                                                                                    id="second_dropdown"
                                                                                    class="form-select">
                                                                                    <option value="0" disabled="true"
                                                                                        selected="true">-Select-
                                                                                    </option>
                                                                                    @foreach ($subc as $kss)
                                                                                        <option
                                                                                            value="{{ $kss->id }}"
                                                                                            <?php if (!empty($p->golongan) && $p->golongan == $kss->id) {
                                                                                                echo 'selected';
                                                                                            } ?>>
                                                                                            {{ $kss->nama }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                        <div class="col-xxl-6">
                                                                            <div>
                                                                                <label class="form-label">Tanggal
                                                                                    SK</label>
                                                                                <input type="date" class="form-control"
                                                                                    name="tanggalsk"
                                                                                    value="{{ $p->tanggalsk }}">
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                        <div class="col-xxl-6">
                                                                            <div>
                                                                                <label class="form-label">TMT
                                                                                    Pangkat</label>
                                                                                <input type="text"
                                                                                    value="{{ $p->tmtpangkat }}"
                                                                                    name="tmtpangkat"
                                                                                    class="form-control">
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                        <div class="col-lg-12">
                                                                            <div class="hstack gap-2 justify-content-end">
                                                                                <button type="button"
                                                                                    class="btn btn-light"
                                                                                    data-bs-dismiss="modal">Close</button>
                                                                                <button type="submit"
                                                                                    class="btn btn-primary">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                    </div>
                                                                    <!--end row-->
                                                                </form>
                                                            </div>

                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                        </div>
                                        <?php } ?>

                                    </div>
                                    <!--end tab-pane-->
                                    <div class="tab-pane" id="jabatan" role="tabpanel">
                                        <div class="mt-4 mb-3 border-bottom pb-2">
                                            <div class="float-end">
                                                <button type="button"
                                                    class="btn btn-ghost-dark waves-effect waves-light btn-sm"
                                                    data-bs-toggle="modal" data-bs-target="#myModal_1">Tambah</button>
                                                <!-- Default Modals -->
                                                <div id="myModal_1" class="modal fade" tabindex="-1"
                                                    aria-labelledby="myModalLabel" aria-hidden="true"
                                                    style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel">Pangkat Baru
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"> </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ route('save_jabatan') }}"
                                                                    method="POST">
                                                                    <div class="row g-3">
                                                                        @csrf
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Jenis
                                                                                    Jabatan</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="jenisjabatan">
                                                                                <input type="hidden" name="id"
                                                                                    value="{{ $hd }}">
                                                                            </div>
                                                                        </div>

                                                                        <!-- Field input Jabatan -->
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Jabatan</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="jabatan">
                                                                            </div>
                                                                        </div>

                                                                        <!-- Field input TMT Jabatan -->
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">TMT
                                                                                    Jabatan</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="tmtjabatan">
                                                                            </div>
                                                                        </div>

                                                                        <!-- Field input Unit Kerja -->
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Unit
                                                                                    Kerja</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="unitkerja">
                                                                            </div>
                                                                        </div>

                                                                        <!--end col-->

                                                                        <!--end col-->
                                                                        <div class="col-lg-12">
                                                                            <div class="hstack gap-2 justify-content-end">
                                                                                <button type="button"
                                                                                    class="btn btn-light"
                                                                                    data-bs-dismiss="modal">Close</button>
                                                                                <button type="submit"
                                                                                    class="btn btn-primary">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                    </div>
                                                                    <!--end row-->
                                                                </form>

                                                            </div>

                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                            <h5 class="card-title">Jabatan History</h5>
                                        </div>
                                        <?php foreach ($UserJabatan as $jb) {?>
                                        <div class="d-flex align-items-center mb-3">
                                            <div class="flex-shrink-0 avatar-sm">
                                                <div class="avatar-title bg-light text-primary rounded-3 fs-18">
                                                    <i class="ri-award-fill"></i>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1 ms-3">
                                                <h6>{{ $jb->jabatan }} / {{ $jb->jenisjabatan }}</h6>
                                                <p class="text-muted mb-0">Unit Kerja : {{ $jb->unitkerja }}
                                                    <br> TMT.Pangkat : {{ $jb->tmtjabatan }}
                                                </p>
                                            </div>
                                            <div>
                                                <button type="button"
                                                    class="btn btn-ghost-dark waves-effect waves-light btn-sm"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#jbd_{{ $jb->id }}">Edit</button>
                                                <a href="{{ route('hapus_jabatan', ['id' => $jb->id, 'userId' => $hd]) }}"
                                                    type="button"
                                                    class="btn btn-ghost-danger waves-effect waves-light btn-sm">Delete</a>

                                                <div id="jbd_{{ $jb->id }}" class="modal fade" tabindex="-1"
                                                    aria-labelledby="myModalLabel" aria-hidden="true"
                                                    style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel">Edit Jabatan
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"> </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ route('update_jabatan') }}"
                                                                    method="POST">
                                                                    <div class="row g-3">
                                                                        @csrf
                                                                        <input type="text" name="id"
                                                                            value="{{ $jb->id }}">
                                                                        <input type="text" name="hd"
                                                                            value="{{ $hd }}">

                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Jenis
                                                                                    Jabatan</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="jenisjabatan"
                                                                                    value="{{ $jb->jenisjabatan }}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Jabatan</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="jabatan"
                                                                                    value="{{ $jb->jabatan }}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">TMT
                                                                                    Jabatan</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="tmtjabatan"
                                                                                    value="{{ $jb->tmtjabatan }}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Unit
                                                                                    Kerja</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="unitkerja"
                                                                                    value="{{ $jb->unitkerja }}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-12">
                                                                            <div class="hstack gap-2 justify-content-end">
                                                                                <button type="button"
                                                                                    class="btn btn-light"
                                                                                    data-bs-dismiss="modal">Close</button>
                                                                                <button type="submit"
                                                                                    class="btn btn-primary">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>

                                                            </div>

                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                        </div>
                                        <?php } ?>

                                    </div>
                                    <!--end tab-pane-->
                                    <div class="tab-pane" id="pendidikan" role="tabpanel">
                                        <div class="mt-4 mb-3 border-bottom pb-2">
                                            <div class="float-end">
                                                <button type="button"
                                                    class="btn btn-ghost-dark waves-effect waves-light btn-sm"
                                                    data-bs-toggle="modal" data-bs-target="#myModal_2">Tambah</button>
                                                <!-- Default Modals -->
                                                <div id="myModal_2" class="modal fade" tabindex="-1"
                                                    aria-labelledby="myModalLabel" aria-hidden="true"
                                                    style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel">Pendidikan Baru
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"> </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ route('save_pendidikan') }}"
                                                                    method="POST">
                                                                    <div class="row g-3">
                                                                        @csrf
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label
                                                                                    class="form-label">Pendidikan</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="pendidikan">
                                                                                <input type="hidden" name="id"
                                                                                    value="{{ $hd }}">
                                                                            </div>
                                                                        </div>

                                                                        <!-- Field input Jabatan -->
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Program
                                                                                    Studi</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="programstudi">
                                                                            </div>
                                                                        </div>

                                                                        <!-- Field input TMT Jabatan -->
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Tahun
                                                                                    Lulus</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="tahunlulus">
                                                                            </div>
                                                                        </div>

                                                                        <!--end col-->

                                                                        <!--end col-->
                                                                        <div class="col-lg-12">
                                                                            <div class="hstack gap-2 justify-content-end">
                                                                                <button type="button"
                                                                                    class="btn btn-light"
                                                                                    data-bs-dismiss="modal">Close</button>
                                                                                <button type="submit"
                                                                                    class="btn btn-primary">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                    </div>
                                                                    <!--end row-->
                                                                </form>

                                                            </div>

                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                            <h5 class="card-title">Pendidikan History</h5>
                                        </div>
                                        <?php foreach ($UserPendidikan as $pdd) {?>
                                        <div class="d-flex align-items-center mb-3">
                                            <div class="flex-shrink-0 avatar-sm">
                                                <div class="avatar-title bg-light text-primary rounded-3 fs-18">
                                                    <i class="ri-book-2-fill"></i>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1 ms-3">
                                                <h6>{{ $pdd->pendidikan }} / {{ $pdd->programstudi }}</h6>
                                                <p class="text-muted mb-0">Tahun Lulus : {{ $pdd->tahunlulus }}
                                            </div>
                                            <div>
                                                <button type="button"
                                                    class="btn btn-ghost-dark waves-effect waves-light btn-sm"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#pdd_{{ $pdd->id }}">Edit</button>
                                                <a href="{{ route('hapus_pendidikan', ['id' => $pdd->id, 'userId' => $hd]) }}"
                                                    type="button"
                                                    class="btn btn-ghost-danger waves-effect waves-light btn-sm">Delete</a>
                                                <div id="pdd_{{ $pdd->id }}" class="modal fade" tabindex="-1"
                                                    aria-labelledby="myModalLabel" aria-hidden="true"
                                                    style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel">Edit Pendidikan
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"> </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ route('update_pendidikan') }}"
                                                                    method="POST">
                                                                    <div class="row g-3">
                                                                        @csrf
                                                                        <input type="text" name="id"
                                                                            value="{{ $pdd->id }}">
                                                                        <input type="text" name="hd"
                                                                            value="{{ $hd }}">

                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label
                                                                                    class="form-label">Pendidikan</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="pendidikan"
                                                                                    value="{{ $pdd->pendidikan }}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Program
                                                                                    Studi</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="programstudi"
                                                                                    value="{{ $pdd->programstudi }}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Tahun
                                                                                    Lulus</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="tahunlulus"
                                                                                    value="{{ $pdd->tahunlulus }}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-12">
                                                                            <div class="hstack gap-2 justify-content-end">
                                                                                <button type="button"
                                                                                    class="btn btn-light"
                                                                                    data-bs-dismiss="modal">Close</button>
                                                                                <button type="submit"
                                                                                    class="btn btn-primary">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>

                                                            </div>

                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                        </div>
                                        <?php } ?>

                                    </div>
                                    <!--end tab-pane-->
                                    <div class="tab-pane" id="diklat" role="tabpanel">
                                        <div class="mt-4 mb-3 border-bottom pb-2">
                                            <div class="float-end">
                                                <button type="button"
                                                    class="btn btn-ghost-dark waves-effect waves-light btn-sm"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#myModal_diklat">Tambah</button>
                                                <!-- Default Modals -->
                                                <div id="myModal_diklat" class="modal fade" tabindex="-1"
                                                    aria-labelledby="myModalLabel" aria-hidden="true"
                                                    style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel">Diklat Baru
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"> </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ route('save_diklat') }}" method="POST">
                                                                    <div class="row g-3">
                                                                        @csrf
                                                                        <input type="hidden" name="id"
                                                                            value="{{ $hd }}">

                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Nama
                                                                                    Diklat</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="namadiklat">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Penyelenggara
                                                                                    Diklat</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="penyelenggaradiklat">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Tanggal
                                                                                    Diklat</label>
                                                                                <input type="date" class="form-control"
                                                                                    name="tanggaldiklat">
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                        <div class="col-lg-12">
                                                                            <div class="hstack gap-2 justify-content-end">
                                                                                <button type="button"
                                                                                    class="btn btn-light"
                                                                                    data-bs-dismiss="modal">Close</button>
                                                                                <button type="submit"
                                                                                    class="btn btn-primary">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                    </div>
                                                                    <!--end row-->
                                                                </form>

                                                            </div>

                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                            <h5 class="card-title">Diklat History</h5>
                                        </div>
                                        <?php foreach ($UserDiklat as $dik) {?>
                                        <div class="d-flex align-items-center mb-3">
                                            <div class="flex-shrink-0 avatar-sm">
                                                <div class="avatar-title bg-light text-primary rounded-3 fs-18">
                                                    <i class="ri-briefcase-fill"></i>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1 ms-3">
                                                <h6>{{ $dik->namadiklat }} / {{ $dik->tanggaldiklat }}</h6>
                                                <p class="text-muted mb-0">Peneyelenggara :
                                                    {{ $dik->penyelenggaradiklat }}
                                                </p>
                                            </div>
                                            <div>
                                                <button type="button"
                                                    class="btn btn-ghost-dark waves-effect waves-light btn-sm"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#diklatt_{{ $dik->id }}">Edit</button>

                                                <a href="{{ route('hapus_diklat', ['id' => $dik->id, 'userId' => $hd]) }}"
                                                    type="button"
                                                    class="btn btn-ghost-danger waves-effect waves-light btn-sm">Delete</a>


                                                <div id="diklatt_{{ $dik->id }}" class="modal fade" tabindex="-1"
                                                    aria-labelledby="myModalLabel" aria-hidden="true"
                                                    style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel">Edit Diklat
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"> </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ route('update_diklat') }}"
                                                                    method="POST">
                                                                    <div class="row g-3">
                                                                        @csrf
                                                                        <input type="text" name="id"
                                                                            value="{{ $dik->id }}">
                                                                        <input type="text" name="hd"
                                                                            value="{{ $hd }}">

                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Nama
                                                                                    Diklat</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="namadiklat"
                                                                                    value="{{ $dik->namadiklat }}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Penyelenggara
                                                                                    Diklat</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="penyelenggaradiklat"
                                                                                    value="{{ $dik->penyelenggaradiklat }}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Tanggal
                                                                                    Diklat</label>
                                                                                <input type="date" class="form-control"
                                                                                    name="tanggaldiklat"
                                                                                    value="{{ $dik->tanggaldiklat }}">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-12">
                                                                            <div class="hstack gap-2 justify-content-end">
                                                                                <button type="button"
                                                                                    class="btn btn-light"
                                                                                    data-bs-dismiss="modal">Close</button>
                                                                                <button type="submit"
                                                                                    class="btn btn-primary">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>

                                                            </div>

                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                        </div>
                                        <?php } ?>

                                    </div>
                                    <!--end tab-pane-->
                                    <div class="tab-pane" id="pak" role="tabpanel">
                                        <div class="mt-4 mb-3 border-bottom pb-2">
                                            <div class="float-end">
                                                <button type="button"
                                                    class="btn btn-ghost-dark waves-effect waves-light btn-sm"
                                                    data-bs-toggle="modal" data-bs-target="#myModal_pak">Tambah</button>
                                                <!-- Default Modals -->
                                                <div id="myModal_pak" class="modal fade" tabindex="-1"
                                                    aria-labelledby="myModalLabel" aria-hidden="true"
                                                    style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel">Pak Baru
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"> </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ route('save_pak') }}" method="POST">
                                                                    <div class="row g-3">
                                                                        @csrf
                                                                        <input type="hidden" name="id"
                                                                            value="{{ $hd }}">

                                                                        <!-- Field input Jumlah Nilai -->
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Jumlah
                                                                                    Nilai</label>
                                                                                <input type="number" class="form-control"
                                                                                    name="jumlahnilai">
                                                                            </div>
                                                                        </div>

                                                                        <!-- Field input Tanggal Penilaian -->
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Tanggal
                                                                                    Penilaian</label>
                                                                                <input type="date" class="form-control"
                                                                                    name="tanggalpenilaian">
                                                                            </div>
                                                                        </div>

                                                                        <!-- Field input Tanggal Penetapan -->
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Tanggal
                                                                                    Penetapan</label>
                                                                                <input type="date" class="form-control"
                                                                                    name="tanggalpenetapan">
                                                                            </div>
                                                                        </div>

                                                                        <!-- Field input Pejabat Penilai -->
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Pejabat
                                                                                    Penilai</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="pejabatpenilai">
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                        <div class="col-lg-12">
                                                                            <div class="hstack gap-2 justify-content-end">
                                                                                <button type="button"
                                                                                    class="btn btn-light"
                                                                                    data-bs-dismiss="modal">Close</button>
                                                                                <button type="submit"
                                                                                    class="btn btn-primary">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                        <!--end col-->
                                                                    </div>
                                                                    <!--end row-->
                                                                </form>

                                                            </div>

                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                            <h5 class="card-title">Pak History</h5>
                                        </div>
                                        <?php foreach ($UserPak as $pak) {?>
                                        <div class="d-flex align-items-center mb-3">
                                            <div class="flex-shrink-0 avatar-sm">
                                                <div class="avatar-title bg-light text-primary rounded-3 fs-18">
                                                    <i class="ri-briefcase-fill"></i>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1 ms-3">
                                                <h6>Jumlah Nilai : {{ $pak->jumlahnilai }}</h6>
                                                <p class="text-muted mb-0">
                                                    <?php
                                                    $tanggalPenilaian = new DateTime($pak->tanggalpenilaian);
                                                    $tanggalFormatted = $tanggalPenilaian->format('d F Y');
                                                    $tanggalpenetapan = new DateTime($pak->tanggalpenetapan);
                                                    $t2 = $tanggalpenetapan->format('d F Y');
                                                    ?>
                                                    Penilaian:{{ $tanggalFormatted }}
                                                    <br>
                                                    Penetapan:{{ $t2 }}
                                                    <br>
                                                    Pejabat Penilai :{{ $pak->pejabatpenilai }}
                                                </p>
                                            </div>
                                            <div>
                                                <button type="button"
                                                    class="btn btn-ghost-dark waves-effect waves-light btn-sm"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#pakk_{{ $pak->id }}">Edit</button>
                                                <a href="{{ route('hapus_pak', ['id' => $pak->id, 'userId' => $hd]) }}"
                                                    type="button"
                                                    class="btn btn-ghost-danger waves-effect waves-light btn-sm">Delete</a>
                                                <div id="pakk_{{ $pak->id }}" class="modal fade" tabindex="-1"
                                                    aria-labelledby="myModalLabel" aria-hidden="true"
                                                    style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel">Edit Pak
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"> </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ route('update_pak') }}" method="POST">
                                                                    <div class="row g-3">
                                                                        @csrf
                                                                        <input type="text" name="id"
                                                                            value="{{ $pak->id }}">
                                                                        <input type="text" name="hd"
                                                                            value="{{ $hd }}">

                                                                        <!-- Field input Jumlah Nilai -->
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Jumlah
                                                                                    Nilai</label>
                                                                                <input type="number" class="form-control"
                                                                                    name="jumlahnilai"
                                                                                    value="{{ $pak->jumlahnilai }}">
                                                                            </div>
                                                                        </div>

                                                                        <!-- Field input Tanggal Penilaian -->
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Tanggal
                                                                                    Penilaian</label>
                                                                                <input type="date" class="form-control"
                                                                                    name="tanggalpenilaian"
                                                                                    value="{{ $pak->tanggalpenilaian }}">
                                                                            </div>
                                                                        </div>

                                                                        <!-- Field input Tanggal Penetapan -->
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Tanggal
                                                                                    Penetapan</label>
                                                                                <input type="date" class="form-control"
                                                                                    name="tanggalpenetapan"
                                                                                    value="{{ $pak->tanggalpenetapan }}">
                                                                            </div>
                                                                        </div>

                                                                        <!-- Field input Pejabat Penilai -->
                                                                        <div class="col-xxl-12">
                                                                            <div>
                                                                                <label class="form-label">Pejabat
                                                                                    Penilai</label>
                                                                                <input type="text" class="form-control"
                                                                                    name="pejabatpenilai"
                                                                                    value="{{ $pak->pejabatpenilai }}">
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-lg-12">
                                                                            <div class="hstack gap-2 justify-content-end">
                                                                                <button type="button"
                                                                                    class="btn btn-light"
                                                                                    data-bs-dismiss="modal">Close</button>
                                                                                <button type="submit"
                                                                                    class="btn btn-primary">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>

                                                            </div>

                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                        </div>
                                        <?php } ?>

                                    </div>
                                    <!--end tab-pane-->

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->

            </div>
            <!-- container-fluid -->
        </div><!-- End Page-content -->

        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> © Nuskhu Digital.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            Design & Develop by Nuskhu Digital
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
<script>
    // Mengambil semua elemen tab yang dapat diklik
    const tabElements = document.querySelectorAll('[role="tabpanel"]');

    // Membuat event listener untuk setiap elemen tab
    tabElements.forEach(tabElement => {
        tabElement.addEventListener('click', () => {
            // Menghapus kelas active dari semua elemen tab
            tabElements.forEach(tab => tab.classList.remove('active'));
            // Menambahkan kelas active pada elemen tab yang diklik
            tabElement.classList.add('active');
        });
    });
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('change', '.productcategory', function() {
            // console.log("hmm its change");

            var cat_id = $(this).val();
            // console.log(cat_id);
            var div = $(this).parent();

            var op = " ";

            $.ajax({
                type: 'get',
                url: '{!! URL::to('get-options') !!}',
                data: {
                    'id': cat_id
                },
                dataType: "json",
                success: function(data) {
                    $('#second_dropdown').empty();
                    $('#second_dropdown').append(
                        '<option value="">Select Golongan</option>');
                    $.each(data, function(key, value) {
                        $('#second_dropdown').append('<option value="' + value.id +
                            '">' + value.nama + '</option>');
                    });
                }

            });
        });
    });
</script>
{{-- --------- --}}
