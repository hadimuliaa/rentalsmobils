@extends('template')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0">
                                Profil </h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Pages</a></li>
                                    <li class="breadcrumb-item active">Profil</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <div class="row">
                    <form method="POST" action="{{ route('ups') }}">
                        <?php foreach ($cred as $k) {?>
                        <div class="row">
                            @csrf
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="nip"
                                            value="{{ $k->nip }}">
                                        <label for="firstnamefloatingInput">NIP</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="nama"
                                            value="{{ $k->name }}">
                                        <label for="firstnamefloatingInput">Nama</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="ForminputState" class="form-label">Pangkat</label>
                                    <select name="cat" id="prod_cat_id" class="form-select productcategory">
                                        <option value="0" disabled="true" selected="true">-Select-</option>
                                        @foreach ($cat as $c)
                                            <option value="{{ $c->id }}" @selected($k->pangkat == $c->id)>
                                                {{ $c->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label class="form-label">Golongan</label>
                                    <select name="subcat" id="second_dropdown" class="form-select">
                                        <option value="0" disabled="true" selected="true">-Select-</option>
                                        @foreach ($subc as $subcatItem)
                                            <option value="{{ $subcatItem->id }}" <?php if (!empty($k->subcat) && $k->subcat == $subcatItem->id) {
                                                echo 'selected';
                                            } ?>>
                                                {{ $subcatItem->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="tmtpangkat"
                                            value="{{ $k->tmtpangkat }}">
                                        <label for="firstnamefloatingInput">TMT Pangkat</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="nomorsk"
                                            value="{{ $k->nomorsk }}">
                                        <label for="firstnamefloatingInput">TMT Nomor SK</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="jabatan"
                                            value="{{ $k->jabatan }}">
                                        <label for="firstnamefloatingInput">Jabatan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="jenisjabatan"
                                            value="{{ $k->jenisjabatan }}">
                                        <label for="firstnamefloatingInput">Jenis Jabatan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="masakerja"
                                            value="{{ $k->masakerja }}">
                                        <label for="firstnamefloatingInput">Masa Kerja</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="dalamjabatan"
                                            value="{{ $k->dalamjabatan }}">
                                        <label for="firstnamefloatingInput">Dalam Jabatan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="pendidikan"
                                            value="{{ $k->pendidikan }}">
                                        <label for="firstnamefloatingInput">Pendidikan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="programstudi"
                                            value="{{ $k->programstudi }}">
                                        <label for="firstnamefloatingInput">Program Studi</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="unitkerja"
                                            value="{{ $k->unitkerja }}">
                                        <label for="firstnamefloatingInput">Unit Kerja</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="alamat"
                                            value="{{ $k->alamat }}">
                                        <label for="firstnamefloatingInput">Alamat</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="email" class="form-control" name="email"
                                            value="{{ $k->email }}">
                                        <label for="firstnamefloatingInput">Email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="nomorhp"
                                            value="{{ $k->nomorhp }}">
                                        <label for="firstnamefloatingInput">Nomor HP</label>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- Add other fields as needed -->

                        <!-- Buttons Grid -->
                        <div class="d-grid gap-2">
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>

                    </form>
                </div>


            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> © Nuskhu Digital.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            Design & Develop by Nuskhu Digital
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('change', '.productcategory', function() {
            // console.log("hmm its change");

            var cat_id = $(this).val();
            // console.log(cat_id);
            var div = $(this).parent();

            var op = " ";

            $.ajax({
                type: 'get',
                url: '{!! URL::to('get-options') !!}',
                data: {
                    'id': cat_id
                },
                dataType: "json",
                success: function(data) {
                    $('#second_dropdown').empty();
                    $('#second_dropdown').append(
                        '<option value="">Select Golongan</option>');
                    $.each(data, function(key, value) {
                        $('#second_dropdown').append('<option value="' + value.id +
                            '">' + value.nama + '</option>');
                    });
                }

            });
        });
    });
</script>
{{-- --------- --}}
