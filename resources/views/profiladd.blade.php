@extends('template')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0">
                                Profil </h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Pages</a></li>
                                    <li class="breadcrumb-item active">Profil</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <div class="row">
                    <form method="POST" action="{{ route('actnewuser') }}">
                        <div class="row">
                            @csrf

                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="nip">
                                        <label for="firstnamefloatingInput">NIP</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="name" name="name">
                                        <label for="name">Nama</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="email" class="form-control" id="email" name="email">
                                        <label for="email">Email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="password" class="form-control" id="password" name="password">
                                        <label for="password">Password</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <textarea class="form-control" id="address" name="alamat"></textarea>
                                        <label for="address">Alamat</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="tel" class="form-control" id="phone" name="nomorhp">
                                        <label for="phone">Nomor HP</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Add other fields as needed -->

                        <!-- Buttons Grid -->
                        <div class="d-grid gap-2">
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>

                    </form>
                </div>


            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> © Nuskhu Digital.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            Design & Develop by Nuskhu Digital
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
