@extends('template')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0">Import Data Pegawai</h5>
                            </div>
                            <div class="card-body">
                                {{-- ---- --}}
                                <form action="{{ route('import.data') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="input-group">
                                        <input type="file" class="form-control" name="excel_file" id="inputGroupFile04"
                                            aria-describedby="inputGroupFileAddon04" aria-label="Upload">
                                        <button class="btn btn-outline-success" type="submit"
                                            id="inputGroupFileAddon04">Import Data</button>
                                    </div>
                                </form>
                                {{-- ---- --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0">List Pegawai</h5>
                            </div>
                            <div class="card-body">
                                <a href="{{ route('addnew') }}" type="button"
                                    class="btn btn-outline-secondary waves-effect waves-light">Tambah
                                    Pegawai</a>

                                <a href="{{ route('export.template') }}" type="button"
                                    class="btn btn-info btn-label waves-effect waves-light"><i
                                        class="ri-file-excel-fill label-icon align-middle fs-16 me-2"></i> Download
                                    Template</a>
                                <hr>
                                <table id="alternative-pagination"
                                    class="table nowrap dt-responsive align-middle table-hover table-bordered"
                                    style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NIP</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>HP</th>
                                            <th colspan="2">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                        foreach ($cred as $k) {?>
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $k->nip }}</td>
                                            <td>{{ $k->name }}</td>
                                            <td>{{ $k->alamat }}</td>
                                            <td>{{ $k->nomorhp }}</td>
                                            <td>
                                                <a href="{{ route('profil', ['id' => $k->id]) }}"
                                                    class="btn btn-sm btn-soft-info">View</a>
                                                <a href="{{ route('hapus_user', ['id' => $k->id]) }}" type="button"
                                                    class="btn btn-ghost-danger waves-effect waves-light btn-sm">Delete</a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->



            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> © Nuskhu Digital.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            Design & Develop by Nuskhu Digital
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
