<?php

use App\Http\Controllers\Nuskhu;
use App\Http\Controllers\Otent;
use App\Http\Controllers\Welcome;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', [Otent::class, 'login'])->name('/');
Route::get('/masuak-min', [Otent::class, 'login'])->name('login');
Route::post('/masuak-min', [Otent::class, 'cek'])->name('postlog');
Route::get('/logout', [Otent::class, 'logout'])->name('logout');

Route::group(['middleware' => ['admin']], function () {
    Route::get('/persetujuan', [Nuskhu::class, 'persetujuan'])->name('persetujuan');
    Route::get('/all-user', [Nuskhu::class, 'all_user'])->name('alluser');
});
Route::group(['middleware' => ['islogin']], function () {
    Route::get('/utamin', [Nuskhu::class, 'index']);

    Route::get('/product/tambah', [Nuskhu::class, 'add_prod'])->name('addprod');
    Route::post('/product', [Nuskhu::class, 'updateProduct'])->name('uprod');

    Route::get('/pelanggan', [Nuskhu::class, 'pelanggan'])->name('pelanggan');
    Route::post('/pelanggan', [Nuskhu::class, 'user_act'])->name('update_user');
    Route::get('/product', [Nuskhu::class, 'product'])->name('product');
    Route::get('/pengembalian', [Nuskhu::class, 'kembalian'])->name('kembalian');
    Route::post('/pengembalian', [Nuskhu::class, 'kembalian_act'])->name('baliakan');
    Route::post('/product/tambah', [Nuskhu::class, 'add_prodsewa_act'])->name('uprodSewa');
    Route::post('/product/tambahh', [Nuskhu::class, 'add_mobil_act'])->name('admobilss');
    Route::get('/products/{id}/delete', [Nuskhu::class, 'destroy'])->name('delete-product');
});
