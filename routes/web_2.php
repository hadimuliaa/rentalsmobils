<?php

use App\Http\Controllers\Nuskhu;
use App\Http\Controllers\Otent;
use App\Http\Controllers\Welcome;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', [Welcome::class, 'index'])->name('/');
Route::get('/', [Otent::class, 'login'])->name('/');
// Route::get('/register', [Welcome::class, 'showRegistrationForm'])->name('register.form');
// Route::post('/register', [Welcome::class, 'register'])->name('register');
// Route::get('/logins', [Welcome::class, 'logins'])->name('logins');
// Route::post('/logins', [Welcome::class, 'mask'])->name('masuak');
// Route::get('/product-knwoledge', [Welcome::class, 'blog'])->name('product-knwoledge');
// Route::get('/product-details/{id}', [Welcome::class, 'details'])->name('details');
Route::get('/masuak-min', [Otent::class, 'login'])->name('login');
Route::post('/masuak-min', [Otent::class, 'cek'])->name('postlog');
Route::get('/logout', [Otent::class, 'logout'])->name('logout');

// Route::group(['middleware' => ['islogin']], function () {
//     Route::get('/dashboard-buyer', [Welcome::class, 'dashboard_user'])->name('dashboa.fe');
//     Route::get('/keranjang-buyer', [Welcome::class, 'cartbuy'])->name('cart');
//     Route::post('/update-profile', [Welcome::class, 'updateProfile'])->name('update-profile');
//     Route::post('/dashboard-buyer', [Welcome::class, 'addToCart'])->name('addToCart');
//     Route::get('/dashboard-buyer/delete/{idcart}', [Welcome::class, 'removeCartItem'])->name('remove-cart-item');
//     Route::post('/dashboard-buyer/update', [Welcome::class, 'updateCartQuantity'])->name('update-cart-quantity');
//     Route::get('checkout', [Welcome::class, 'checkOut'])->name('cekot');
//     Route::get('/get-provinces', [Welcome::class, 'provinsi']);
//     Route::get('/get-subdistrict', [Welcome::class, 'getSubdistrict']);
//     Route::post('/simpan-alamat', [Welcome::class, 'simpanAlamat']);
//     Route::get('/get-kota', [Welcome::class, 'getKota'])->name('get-kota');
//     Route::post('/get-cost', [Welcome::class, 'calculateShippingCost'])->name('get-cost');
//     Route::post('/process-order', [Welcome::class, 'processOrder'])->name('process-order');
//     Route::get('/finish-order', [Welcome::class, 'finish'])->name('finish');
// });
Route::group(['middleware' => ['admin']], function () {
    Route::get('/utamin', [Nuskhu::class, 'index']);
    Route::get('/pelanggan', [Nuskhu::class, 'pelanggan'])->name('pelanggan');
    Route::get('/admin', [Nuskhu::class, 'admin'])->name('admin');
    Route::post('/admin', [Nuskhu::class, 'admin_act'])->name('update_admin');

    Route::get('/harga', [Nuskhu::class, 'harga'])->name('har.go');
    Route::POST('/harga/update', [Nuskhu::class, 'update_harga'])->name('har.go.up');

    Route::get('/pesanan', [Nuskhu::class, 'masuk'])->name('pes.mask');
    Route::get('/masuk-detail', [Nuskhu::class, 'detailmasuk'])->name('detail.mask');
    Route::post('/tuka', [Nuskhu::class, 'updateBuktiPengiriman'])->name('tuka');
    Route::get('/laporan', [Nuskhu::class, 'lap'])->name('lap.trx');

    Route::get('/categori', [Nuskhu::class, 'categori'])->name('categori');
    Route::post('/categori', [Nuskhu::class, 'update_cat'])->name('upcat');
    Route::post('/categori/store', [Nuskhu::class, 'storeCategory'])->name('store-category');
    Route::delete('/delete-category/{idseo}', [Nuskhu::class, 'deleteCategory'])->name('delete-category');

    Route::get('/testi', [Nuskhu::class, 'testi'])->name('testi');
    Route::get('/knowledge', [Nuskhu::class, 'knowledge'])->name('know');
    Route::post('/knowledge', [Nuskhu::class, 'insertKnowledge'])->name('know_insert');
    Route::get('/delete-know/{id}', [Nuskhu::class, 'deleteKnowledge'])->name('delete-know');

    Route::get('/hero', [Nuskhu::class, 'hero'])->name('hero');
    Route::get('/product', [Nuskhu::class, 'product'])->name('product');
    Route::post('/product', [Nuskhu::class, 'updateProduct'])->name('uprod');
    Route::get('/product/tambah', [Nuskhu::class, 'add_prod'])->name('addprod');
    Route::post('/product/tambah', [Nuskhu::class, 'add_prod_act'])->name('simpan-product');
    Route::get('/products/{id}/delete', [Nuskhu::class, 'destroy'])->name('delete-product');
});
